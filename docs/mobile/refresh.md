# Refresh下拉上拉刷新
<phone src="/phones/refresh.html"></phone>
```html
<template>
  <avue-refresh :page.sync="page" @on-load="onLoad" success-text="刷新成功" finished-text="没有更多了">
    <van-cell v-for="item in list" :key="item" :title="item" />
  </avue-refresh>
</template>
<script>
export default {
  data() {
    return {
      list: [],
      page: 1
    }
  },
  methods: {
    onLoad(loading, done) {
      setTimeout(() => {
        for (let i = 0; i < 10; i++) {
          this.list.push(this.list.length + 1);
        }
        loading()
        // 数据全部加载完成
        if (this.list.length >= 40) {
          done();
        }
      }, 1000);
    }
  }
}
</script>
```