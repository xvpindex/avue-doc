# Cell单元格
<phone src="/phones/crud.html"></phone>

```html
<template>
  <avue-crud :option="option" :data="data" :page.sync="page" @on-load="onLoad">

    </avue-crud>
</template>
<script>
export default {
    data() {
      return {
        page: {
          total: 0
        },
        data: [],
        option: {
          // emptyText: "没有发现数据",
          props: {
            title: 'name',
            value: 'sex',
            label: 'phone'
          },
          column: [{
            label: '姓名',
            prop: 'name'
          }, {
            label: '性别',
            prop: 'sex'
          }, {
            label: '手机号',
            prop: 'phone'
          }, {
            label: '字典测试1',
            prop: 'select1',
            type: 'select',
            dicData: [{
              label: '字典1',
              value: 0
            }, {
              label: '字典2',
              value: 1
            }]
          }, {
            label: '字典测试2',
            prop: 'select2',
            type: 'select',
            dicData: [{
              label: '字典1',
              value: 0
            }, {
              label: '字典2',
              value: 1
            }]
          }, {
            label: '创建日期',
            prop: 'createdDate',
            slot: true,
            span: 24
          }, {
            label: '更新日期',
            prop: 'updateDate',
            span: 24
          }]
        }
      }
    },
    methods: {
      onLoad() {
        this.page.total = 40;
        this.data = new Array(10).fill({
          name: 'smallwei1',
          sex: '男',
          select1: 0,
          select2: 1,
          phone: '17547400800',
          createdDate: '2020-02-02 02:02:02',
          updateDate: '2020-02-02 02:02:02'
        })
      }
    }
  }
</script>
```