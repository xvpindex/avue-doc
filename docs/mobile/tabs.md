# Form表单
<phone src="/phones/tabs.html"></phone>

```html
<template>
  <avue-form :option="option" v-model="obj" @submit="submit"></avue-form>
</template>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
export default {
  data() {
    return {
      obj: {
      },
      option: {
        tabs: true,
        column: [{
          label: '主表单',
          prop: 'text'
        }],
        group: [{
          label: '选项卡1',
          icon: 'van-icon-location-o',
          arrow: false,
          column: [
            {
              // prefixIcon: 'contact',
              // suffixIcon: 'question',
              label: '姓名',
              prop: 'name',
              rules: [
                {
                  required: true,
                  message: '请选择姓名',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '密码',
              prop: 'password',
              type: 'password',
              rules: [
                {
                  required: true,
                  message: '请选择密码',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '省份',
              prop: 'province',
              type: 'select',
              props: {
                label: 'name',
                value: 'code'
              },
              cascaderItem: ['city', 'area'],
              dicUrl: `${baseUrl}/getProvince`,
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '城市',
              prop: 'city',
              type: 'select',
              props: {
                label: 'name',
                value: 'code'
              },
              dicUrl: `${baseUrl}/getCity/{{key}}`,
              rules: [
                {
                  required: true,
                  message: '请选择城市',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '地区',
              prop: 'area',
              type: 'select',
              props: {
                label: 'name',
                value: 'code'
              },
              dicUrl: `${baseUrl}/getArea/{{key}}`,
              rules: [
                {
                  required: true,
                  message: '请选择地区',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '开关',
              prop: 'switch',
              type: 'switch',
              dicData: [
                {
                  label: '关闭',
                  value: 0
                },
                {
                  label: '启动',
                  value: 1
                }
              ],
              rules: [
                {
                  required: true,
                  message: '请选择开关',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '日期时间',
              prop: 'datetime',
              type: 'datetime',
              format: 'yyyy年MM月dd日 hh时mm分ss秒',
              rules: [
                {
                  required: true,
                  message: '请选择文本',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '日期',
              prop: 'date',
              type: 'date',
              valueFormat: "timestamp",
              format: 'yyyy年MM月dd日',
              rules: [
                {
                  required: true,
                  message: '请选择日期',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '时间',
              prop: 'time',
              type: 'time',
              format: 'HH小时mm分',
              rules: [
                {
                  required: true,
                  message: '请选择时间',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '数字',
              type: 'number',
              prop: 'number',
              rules: [
                {
                  required: true,
                  message: '请输入数字',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '滑动',
              type: 'slider',
              prop: 'slider',
              rules: [
                {
                  required: true,
                  message: '请选择滑动',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '评价',
              type: 'rate',
              prop: 'rate',
              rules: [
                {
                  required: true,
                  message: '请选择评价',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '文件上传',
              type: 'upload',
              prop: 'upload',
              filename: 'file',
              action: 'https://wlj.wulanchabu.gov.cn//file_api/file/file/addFile',
              propsHttp: {
                home: 'https://wlj.wulanchabu.gov.cn//file_api/file/file/getFile?id=',
                url: 'data'
              },
              data: {
                flag: 2
              },
              rules: [
                {
                  required: true,
                  message: '请上传文件',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '单选',
              prop: 'radio',
              type: 'radio',
              tip: '这是一个小提示',
              tags: true,
              dicData: [
                {
                  label: '单程',
                  value: 0
                },
                {
                  label: '往返',
                  value: 1
                }
              ],
              rules: [
                {
                  required: true,
                  message: '请选择单选',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '多选',
              prop: 'checkbox',
              type: 'checkbox',
              tags: true,
              dicData: [
                {
                  label: '飞机',
                  value: 1
                },
                {
                  label: '火车',
                  value: 2
                },
                {
                  label: '动车',
                  value: 3
                },
                {
                  label: '其他',
                  value: 4
                }
              ],
              rules: [
                {
                  required: true,
                  message: '请选择多选',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '文本',
              prop: 'textarea',
              type: 'textarea',
              minRows: 3,
              maxRows: 4,
              rules: [
                {
                  required: true,
                  message: '请选择文本',
                  trigger: 'blur'
                }
              ]
            }
          ]

        }, {
          label: '选项卡2',
          icon: 'van-icon-like-o',
          column: [{
            label: '输入框',
            prop: 'text'
          }]
        }]
      }
    }
  },
  mounted() {
    setTimeout(() => {
      // this.obj = {
      //   img: [
      //     'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?a=1',
      //     'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?a=2'
      //   ],
      //   password: '111',
      //   province: '130000',
      //   city: '130100',
      //   area: '130101',
      //   radio: 0,
      //   checkbox: [1, 3],
      //   switch: 1,
      //   datetime: '2020-10-10 10:10:10',
      //   date: 1546838748000,
      //   time: '03:22'
      // }
    }, 0)
  },
  methods: {
    submit() {
      this.$toast(JSON.stringify(this.obj))
    }
  }
}
</script>
```