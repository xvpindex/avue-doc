# Swipe轮播
<phone src="/phones/swipe.html"></phone>
```html
<template>
  <avue-swipe :option="option" @click="handleClick"></avue-swipe>
</template>
<script>
export default {
  data() {
    return {
      option: {
        // autoplay:5000,
        // loop:false,
        list: [{
          title: '这是标题2',
          url: '/vant/mobile.htm',
          value: 'https://img.yzcdn.cn/vant/apple-1.jpg',
        }, {
          title: '这是标题3',
          url: '/vant/mobile.htm',
          value: 'https://img.yzcdn.cn/vant/apple-2.jpg',
        }]
      }
    }
  },
  methods: {
    handleClick(item) {
      console.log(item)
    }
  }
}
</script>
```