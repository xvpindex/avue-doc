# Card卡片列表
<phone src="/phones/card.html"></phone>
```html
<template>
  <avue-card :data="data" @click="handleClick" @click-thumb="handleClickThumb">
    <div slot="tags" slot-scope="scope">
      <van-tag plain type="danger">标签1</van-tag>
      &nbsp;
      <van-tag plain type="danger">标签2</van-tag>
    </div>
    <div slot="footer" slot-scope="scope">
      <van-button size="mini">按钮</van-button>
      <van-button size="mini">按钮</van-button>
    </div>
  </avue-card>
</template>
<script>
export default {
  data() {
    return {
      data: [{
        price: "2.00",
        desc: "描述信息",
        title: "商品标题",
        tag1: '标签1',
        tag2: '标签2',
        thumb: "https://img.yzcdn.cn/vant/ipad.jpeg"
      }, {
        price: "2.00",
        desc: "描述信息",
        title: "商品标题",
        tag1: '标签1',
        thumb: "https://img.yzcdn.cn/vant/ipad.jpeg"
      }, {
        price: "2.00",
        desc: "描述信息",
        title: "商品标题",
        thumb: "https://img.yzcdn.cn/vant/ipad.jpeg"
      }],
    }
  },
  methods: {
    handleClickThumb(item) {
      console.log(item)
    },
    handleClick(item) {
      console.log(item)
    }
  }
}
</script>
```