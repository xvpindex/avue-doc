# Grid菜单
<phone src="/phones/grid.html"></phone>
```html
<template>
  <avue-grid :option="option" @click="handleClick"></avue-grid>
</template>
<script>
export default {
  data() {
    return {
      option: {
        // span: 12,
        // round: 10,
        list: [{
          url: '/vant/mobile.htm',
          title: '我是菜单1',
          color: '#67C23A',
          url: '/vant/mobile.htm',
          icon: 'like-o'
        }, {
          url: '/vant/mobile.htm',
          title: '我是菜单2',
          color: '#409EFF',
          url: '/vant/mobile.htm',
          icon: 'star-o'
        }, {
          url: '/vant/mobile.htm',
          title: '我是菜单3',
          color: '#E6A23C',
          url: '/vant/mobile.htm',
          icon: 'phone-o'
        }, {
          url: '/vant/mobile.htm',
          title: '我是菜单4',
          color: '#F56C6C',
          url: '/vant/mobile.htm',
          icon: 'flag-o'
        }, {
          url: '/vant/mobile.htm',
          title: '我是菜单5',
          color: '#F56C6C',
          url: '/vant/mobile.htm',
          icon: 'flower-o'
        }, {
          url: '/vant/mobile.htm',
          title: '我是菜单6',
          color: '#E6A23C',
          url: '/vant/mobile.htm',
          icon: 'brush-o'
        }, {
          url: '/vant/mobile.htm',
          title: '我是菜单7',
          color: '#67C23A',
          url: '/vant/mobile.htm',
          icon: 'bulb-o'
        }, {
          url: '/vant/mobile.htm',
          title: '我是菜单8',
          color: '#409EFF',
          url: '/vant/mobile.htm',
          icon: 'user-o'
        }]
      }
    }
  },
  methods: {
    handleClick(item) {
      console.log(item)
    }
  }
}
</script>
```