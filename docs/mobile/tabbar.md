# Tabbar标签栏
<phone src="/phones/tabbar.html"></phone>
```html
<template>
  <avue-tabbar :option="option" @click="handleClick"></avue-tabbar>
</template>
<script>
export default {
  data() {
    return {
      option: {
        // span: 12,
        // round: 10,
        list: [{
          url: '/vant/mobile.htm',
          title: '菜单1',
          color: '#67C23A',
          url: '/vant/mobile.htm',
          icon: 'like-o'
        }, {
          url: '/vant/mobile.htm',
          title: '菜单2',
          color: '#409EFF',
          url: '/vant/mobile.htm',
          icon: 'star-o'
        }, {
          url: '/vant/mobile.htm',
          title: '菜单3',
          color: '#E6A23C',
          url: '/vant/mobile.htm',
          icon: 'phone-o'
        }, {
          url: '/vant/mobile.htm',
          title: '菜单4',
          color: '#F56C6C',
          url: '/vant/mobile.htm',
          icon: 'flag-o'
        }]
      }
    }
  },
  methods: {
    handleClick(item) {
      console.log(item)
    }
  }
}
</script>
```