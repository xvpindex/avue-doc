# 快速上手

:::tip
目前属于开发阶段，开放时间待定，不定期更新模板组件
::::
### 基础
采用[Vant框架](https://vant-contrib.gitee.io/vant/#/zh-CN/)

### 导入
:::tip
min是生产包，另一个则是开发包
::::
  - avue-mobile.js
  - avue-mobile.min.js

### cdn方式
> 将下载的包放入public目录下新建的lib下

``` html

//在index.html引入avue的包
//avue-mobile.min.js为压缩混淆包
//avue-mobile.js为没有压缩混淆的包
<link rel="stylesheet" href="/lib/index.css" />
<script src="/lib/avue-mobule.min.js"></script>

//在main.js中使用
Vue.use(window.AVUE);

```

### npm方式
> 推荐使用 npm 的方式安装，它能更好地和 webpack 打包工具配合使用。

```bash
npm i @smallwei/avue -S
```

```javascript
import Avue from '@smallwei/avue/lib/avue-mobile.min.js';
import '@smallwei/avue/lib/index.css';
Vue.use(Avue);

//如果使用字典需要赋值axios为全局
import axios from 'axios';
window.axios=axios
```







