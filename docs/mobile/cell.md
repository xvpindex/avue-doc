# Cell单元格
<phone src="/phones/cell.html"></phone>

```html
<template>
  <avue-cell :option="option" @click="handleClick">
    <div slot="text2" slot-scope="{item}" style="padding:30px;text-align: center;">
      自定义内容
    </div>
    <div slot="text2Value" slot-scope="{item}">
      自定义Value
    </div>
    <div slot="text2Title" slot-scope="{item}">
      自定义Title
    </div>
    <div slot="text2Label" slot-scope="{item}">
      自定义Label
    </div>
    <div slot="text2Icon" slot-scope="{item}">
      自定义Icon
    </div>
   
  </avue-cell>
</template>
<script>
export default {
  data() {
    return {
      option: {
        // size: 'large',
        center: true,
        data: [{
          label: '内容展示',
          prop: 'group1',
          column: [{
            title: '我是标题',
            prop: 'text1',
            value: '我是内容',
            center: false,
            label: '我是描述信息',
            children: {
              span: 24,
              column: [{
                title: '我是label1',
                prop: 'label1',
                slot: true,
                value: '我是value1',
              }, {
                title: '我是label2',
                slot: true,
                prop: 'label2',
                value: '我是value2',
              }, {
                title: '我是label3',
                prop: 'label3',
                value: '我是value3',
              }]
            },
          }]
        }, {
          label: '颜色菜单',
          prop: 'group2',
          column: [{
            title: '菜单3',
            value: '内容3',
            isLink: true,
            color: '#409EFF',
            prop: 'text3',
            url: '/vant/mobile.htm',
            icon: 'star-o'
          }, {
            title: '菜单4',
            value: '内容4',
            isLink: true,
            color: '#E6A23C',
            prop: 'text4',
            url: '/vant/mobile.htm',
            icon: 'phone-o'
          }, {
            title: '菜单5',
            value: '内容5',
            isLink: true,
            color: '#F56C6C',
            prop: 'text5',
            url: '/vant/mobile.htm',
            icon: 'flag-o'
          }]
        }, {
          label: '简约图标',
          prop: 'group3',
          column: [{
            title: '菜单6',
            value: '内容6',
            isLink: true,
            prop: 'text6',
            url: '/vant/mobile.htm',
            icon: 'star-o'
          }, {
            title: '菜单7',
            value: '内容7',
            isLink: true,
            prop: 'text7',
            url: '/vant/mobile.htm',
            icon: 'phone-o'
          }, {
            title: '菜单8',
            value: '内容8',
            isLink: true,
            prop: 'text8',
            url: '/vant/mobile.htm',
            icon: 'flag-o'
          }]
        }, {
          label: '自定义内容',
          prop: 'group4',
          column: [{
            title: '菜单',
            value: '内容',
            label: '描述',
            prop: 'text2',
            icon: 'star-o'
          }]
        }]
      }
    }
  },
  methods: {
    handleClick(item) {
      console.log(item)
    }
  }
}
</script>
```