# inputNumber数字框

:::demo 
```html
<el-row :span="24">
  <el-col :span="6">
   值:{{form}}<br/>
   <avue-input-number v-model="form"></avue-input-number>
  </el-col>
  <el-col :span="24"></el-col>
  <el-col :span="6">
   值:{{form}}<br/>
    <avue-input-number v-model="form" precision="2" controls-position=""></avue-input-number>
  </el-col>
  <el-col :span="24"></el-col>
  <el-col :span="6">
    有最小最大值的数据:{{hasMinMaxForm}}<br/>
    <avue-input-number v-model="hasMinMaxForm" precision="2" controls-position="" :min-rows="minRows" :max-rows="maxRows"></avue-input-number>
  </el-col>
</el-row>

<script>
export default {
    data() {
      return {
        form:1,
        hasMinMaxForm: 1,
        minRows: 0, // 最小值
        maxRows: 10 // 最大值
      }
    }
}
</script>

```
:::

## Props

| 参数               | 说明                                              | 类型    | 可选值 | 默认值    |
| ------------------ | ------------------------------------------------- | ------- | ------ | --------- |
| min-rows           | 设置计数器允许的最小值                            | number  | —      | -Infinity |
| max-rows           | 设置计数器允许的最大值                            | number  | —      | Infinity  |
| placeholder(label) | 输入框关联的label文字<br />输入框默认 placeholder | string  | —      | —         |
| controls           | 是否使用控制按钮                                  | boolean | —      | true      |
| step               | 计数器步长                                        | number  | —      | 1         |
| controls-position  | 控制按钮位置                                      | string  | right  | —         |
| precision          | 数值精度                                          | number  | —      | —         |
| disabled           | 是否禁用计数器                                    | boolean | —      | false     |

注意事项： `label` 与 `placeholder` 属性在avue中被合并为 `placeholder`

## Events

| 事件名称 | 说明                        | 回调参数       |
| -------- | --------------------------- | -------------- |
| click    | 在组件 Input 点击时触发     | (event: Event) |
| blur     | 在组件 Input 失去焦点时触发 | (event: Event) |
| focus    | 在组件 Input 获得焦点时触发 | (event: Event) |

