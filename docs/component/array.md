# array 数组输入框

:::demo

```html
<el-row :span="24">
  <el-col :span="6">
    值:{{form}}<br />
    <avue-array v-model="form" placeholder="请输入内容"></avue-array>
  </el-col>
</el-row>
<script>
  export default {
    data() {
      return {
        form: ['第一条', '第二条', '第三条', '第四条'],
      }
    },
  }
</script>
```

:::

---

## Props

| 参数               | 说明                                                | 类型    | 可选值 | 默认值    |
| ------------------ | --------------------------------------------------- | ------- | ------ | --------- |
| alone           | 单个模式，禁止使用添加删除功能 | boolean | true/false | false |
| type       | 数组类型，可选值有array, url, img<br />url：鼠标悬浮于输入框可显示跳转提示<br />img：鼠标悬浮于输入框时可显示图片预览 | string | array/url/img | array |
| size        | 修改组件尺寸**（存在问题，修改后按钮发生变形）** | string  | medium/small/mini | —         |
| placeholder | 所有默认框默认的占位字符                       | string | —      | —    |
| readonly       | 数组禁止使用添加删除功能 | boolean | true/false | false    |
| disabled           | 数组禁用修改已有内容、禁止添加删除                  | boolean | true/false | false |
| value | 字典的值属性值 | string | — | — |

