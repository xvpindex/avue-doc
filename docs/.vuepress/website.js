export default {
  //七牛oss配置
  QINIU: {
    AK: '',
    SK: '',
    scope: '',
    url: ''
  },
  //阿里oss配置
  ALI: {
    region: '',
    endpoint: '',
    accessKeyId: '',
    accessKeySecret: '',
    bucket: '',
  }
}