# 全局api
在avue中内置封装了很多全局的api函数，在项目开发中常用的函数，在引入avue后你可以在任何组件里取调用这些方法


### Dialog弹框样式

全局样式`avue-dialog`同时添加`v-dialogdrag`指令即可拖动
:::demo
```html
<el-dialog title="提示"
            v-dialogdrag
           :visible.sync="type0"
            width="40%">
  <span>这是一段信息</span>
  <span slot="footer">
    <el-button @click="type0=false" >取 消</el-button>
    <el-button @click="type0=false" type="primary">确 定</el-button>
  </span>
</el-dialog>
<el-dialog title="提示"
            v-dialogdrag
           :visible.sync="type1"
            class="avue-dialog avue-dialog--top"
            width="40%">
    <span>这是一段信息</span>
    <div class="avue-dialog__footer">
      <el-button @click="type1=false">取 消</el-button>
      <el-button @click="type1=false" type="primary">确 定</el-button>
    </div>
</el-dialog>
<el-button @click="openBox(0)">原生el样式弹窗</el-button>
<el-button type="primary"
            @click="openBox(1)">avue样式弹窗</el-button>

<script>
export default {
  data () {
    return {
      type0: false,
      type1:false
    }
  },
  methods: {
    openBox (index) {
      this['type'+index] = true;
    }
  }
}
</script>
```
:::


### $Clipboard复制到剪切板
可以赋值任意文本到剪切板[在线例子](/doc/clipboard)

``` javascript
 this.$Clipboard({
    text: '复制的文本内容'
  }).then(() => {
    this.$message.success('复制成功')
  }).catch(() => {
    this.$message.error('复制失败')
  });
```

### $ImagePreview图片预览
可以赋值任赋值图片去放大预览(一张缩略图，一张放大图)[在线例子](/doc/image-preview)

``` javascript
 data() {
    var link = 'https://lokeshdhakar.com/projects/lightbox2/images/';
    return {
      datas: [
        { thumbUrl: `${link}thumb-4.jpg`, url: `${link}image-4.jpg` },
        { thumbUrl: `${link}thumb-5.jpg`, url: `${link}image-5.jpg` },
        { thumbUrl: `${link}thumb-6.jpg`, url: `${link}image-6.jpg` },
      ]
    }
  }
 this.$ImagePreview(this.datas, index);
```



### $Print 打印插件
可以传入dom的id或者class[在线例子](/doc/print)
``` javascript
  <div id="test"></div>
  this.$Print('#test')
```

### $Export excel导出
传入相关的属性配置[在线例子](/doc/export)
``` javascript
 let opt = {
    title: '文档标题',
    column: [{
      label: '标题',
      prop: 'title'
    }],
    data: [{
      title: "测试数据1"
    }, {
      title: "测试数据2"
    }]
  }
  this.$Export.excel({
    title: opt.title || new Date().getTime(),
    columns: opt.column,
    data: opt.data
  });
```
### $Log日志打印
可以打印不同颜色和标注的日志

``` javascript
/**
* 内置5中常用颜色,默认为primary
* default:#35495E
* primary:#3488ff
* success:#43B883
* warning:#e6a23c
* danger:#f56c6c
* 也可以直接打印彩色文字
*/

this.$Log.capsule('标题','内容','primary')
this.$Log.primary('内容')
```
![](/images/logs.jpeg)


### findObject发现结构对象

可对对象和数组深拷贝
``` javascript
  var option = {column:[]}
  var prop = this.findObject(option.column,'prop');
  console.log(prop)//操作对象
```


### watermark全局水印
传入水印的文案[在线例子](/doc/watermark)
``` javascript
  this.watermark('我是水印的文案');
```

### downFile文件下载
``` javascript
  var url = "https://avuejs.com/images/logo-bg.jpg";
  this.downFile(url,'logo.jpg');
```

### loadScript
``` javascript
  loadScript('js','xxx.js').then(()=>{
    //执行后的方法
  })
  loadScript('css','xxx.css').then(()=>{
    //执行后的方法
  })
```

### deepClone对象深拷贝
可对对象和数组深拷贝
``` javascript
  var obj1 = {
    name:'张三'
  }
  var obj2 = this.deepClone(obj1);
```

### setPx设置css像素方法
如果传入是数字默认加px属性
``` javascript
  var obj = 23
  console.log(this.setPx(obj)) //'23px'
  console.log(this.setPx('100%')) //'100%'
  console.log(this.setPx('23px')) //'23px'
```

### validatenull判断空
可以判断对象、数组、字符串是否为空
``` javascript
  var obj1 = {}
  var obj2 = []
  var str1 = ''
  var str2 = undefined
  var str3 = null;
  console.log(this.validatenull(obj1)) //true
  console.log(this.validatenull(obj2)) //true
  console.log(this.validatenull(str1)) //true
  console.log(this.validatenull(str2)) //true
  console.log(this.validatenull(str3)) //true
```
### findArray数组中寻找对象下标
> 根据对象属性在数组中找到符合的下表返回，没有则为-1
``` javascript
  var list = [{
    prop:'name'
  },{
    prop:'sex'
  }]
  var index = this.findArray(list,'sex','prop');
  console.log(index) //1
```

### vaildData验证对象
验证是否为空的任何类型数据,为空择取默认的设置值
``` javascript
  var obj = {}
  console.log(this.vaildData(obj,'默认值')) //默认值
  console.log(this.vaildData(obj,{name:11})) //{name:11}
  var obj2 = true
  console.log(this.vaildData(obj2,false)) //true
```