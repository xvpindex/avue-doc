# 快速上手

### 介绍
通过本章节你可以了解到 Avue 的安装方法和基本使用姿势。


### 安装
##### 通过 npm 安装
在现有项目中使用 Avue 时，可以通过 npm 或 yarn 进行安装：

``` bash
#安装
npm i @smallwei/avue -S
yarn add @smallwei/avue -S

# 引入
import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
Vue.use(Avue);

```



##### 通过 CDN 安装
使用 Avue 最简单的方法是直接在 html 文件中引入 CDN 链接(引入的是最新的Avue版本，当然你也可以制定版本号)，之后你可以通过全局变量 AVUE 访问到所有组件。

``` html
<!-- 引入样式文件 -->
<link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/@smallwei/avue/lib/index.css"
/>

<!-- 引入 Vue 和 Avue 的 JS 文件 -->
<script src="https://cdn.jsdelivr.net/npm/next"></script>
<script src="https://cdn.jsdelivr.net/npm/@smallwei/avue/lib/avue.min.js"></script>

<script>
  // 在 #app 标签下渲染一个按钮组件
  new Vue({
    el:'app',
    template: ``,
  });
  app.use(AVUE);
</script>
```
你可以通过以下免费 CDN 服务来使用 Avue:

[jsdelivr](https://www.jsdelivr.com/package/npm/@smallwei/avue)
[unpkg](https://unpkg.com)


### 通过脚手架安装
在新项目中使用 Avue 时，推荐使用 Vue 官方提供的脚手架 Vue Cli 创建项目并安装 Avue

``` bash
# 安装 Vue Cli
npm install -g @vue/cli

# 创建一个项目
vue create hello-world

# 创建完成后，可以通过命令打开图形化界面，如下图所示
vue ui

在图形化界面中，点击 依赖 -> 安装依赖，然后将 @smallwei/avue 添加到依赖中即可。
```