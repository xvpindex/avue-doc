# 单选

:::tip
 2.1.0+
::::


## 普通用法
:::demo  
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {
          radio:0
        },
        option: {
          column: [{
              label: '单选',
              prop: 'radio',
              type: 'radio',
              span:24,
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              },{
                label:'未知',
                value:''
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::



## 按钮样式
:::demo  配置`button`为`true`
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {
          radio:0
        },
        option: {
          column: [{
              label: '实心单选',
              prop: 'radio',
              type: 'radio',
              span:24,
              button:true,
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              },{
                label:'未知',
                value:''
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::



## 空心样式
:::demo  配置`border`为`true`
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {
          radio:0
        },
        option: {
          column: [{
              label: '空心单选',
              prop: 'radio',
              span:24,
              type: 'radio',
              border:true,
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              },{
                label:'未知',
                value:''
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::
