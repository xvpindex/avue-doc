# 组件事件
:::tip
2.6.0+
::::

- `change`事件
- `click`事件
- `focus`事件
- `blur`事件
- `enter`事件

## 普通用法 
:::demo  目前组件有4个事件`change`,`click`,`focus`,`blur`
```html
 <avue-form :option="option" v-model="obj"></avue-form>
<script>
export default {
 data() {
      return {
        obj: {
          name: '11'
        },
        data: [],
        option: {
          column: [{
            label: '姓名',
            prop: 'name',
            change: ({value,column}) => {
              this.$message.success('change事件查看控制台')
              console.log('值改变',value,column)
            },
            click: ({value,column}) => {
              this.$message.success('click事件查看控制台')
              console.log('点击事件',value,column)
            },
            focus: ({value,column}) => {
              this.$message.success('focus事件查看控制台')
              console.log('获取焦点',value,column)
            },
            blur: ({value,column}) => {
              this.$message.success('blur事件查看控制台')
              console.log('失去焦点',value,column)
            },
            enter: ({value,column}) => {
              this.$message.success('enter事件查看控制台')
              console.log('回车事件',value,column)
            }
          }]
        }
      }
    }
}
</script>

```
:::

