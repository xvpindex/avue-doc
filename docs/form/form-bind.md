# 深结构绑定
:::tip
 2.6.11+
::::

## 普通用法 
:::demo  `bind`绑定的对象数据是双向的，改变任意一个，另外一个也会改变
```html
<el-button type="primary" size="small" @click="form.deep.deep.deep.value='改变deep.deep.deep.value值'">改变deep.deep.deep.value值</el-button>
<el-button type="primary" size="small" @click="form.test='改变test值'">改变test值</el-button>
</br></br>
{{form}}
<avue-form :option="option" v-model="form" > </avue-form>

<script>
export default{
  data() {
    return {
      form: { 
        deep:{
          deep:{
            deep:{
              value:1
            }
          }
        }
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '我是深结构',
            prop: 'test',
            bind:'deep.deep.deep.value'
          }
        ]
      }
    }
  }
}
</script>

```
:::
