# 布局设置


## 栏距离
:::demo  设置`gutter`属性调节栏之间的距离
```html
<avue-slider v-model="gutter"></avue-slider>
<avue-form :option="option" v-model="form" @submit="handleSubmit"></avue-form>
<script>
export default {
  data(){
    return {
       gutter:20,
       form:{}
    }
  },
  computed:{
    option(){
      return {
        card: false,
        labelWidth: 110,
        gutter:this.gutter,
        column: [
          {
            label: '姓名',
            prop: 'name',
          },
          {
            label: '默认值',
            prop: 'default',
          },
          {
            label: '数字',
            prop: 'number',
            type: 'number',
            rules: [
              {
                required: true,
                message: '请输入数字',
                trigger: 'blur'
              }
            ]
          }
        ]
       }
    }
  },
  methods:{
    handleSubmit(form){
       this.$message.success(JSON.stringify(this.form))
    }
  }
}
</script>

```
:::

## 行内表单

:::demo  
```html
<avue-slider :min="1" :max="24" v-model="span"></avue-slider>
<avue-form v-model="form" :option="option"></avue-form>

<script>
export default {
    data() {
      return {
        span:6,
        form:{},
      }
    },
    computed:{
      option(){
        return {
          menuSpan:6,
          column: [{
            label: "用户名",
            prop: "username",
            span:this.span,
            rules: [{
                required: true,
                message: "请输入用户名",
                trigger: "blur"
            }]
          }]
        }
      }
    }
  }
</script>
```
:::

