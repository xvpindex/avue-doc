# 级联选择器

## 普通用法

:::demo 
```html
<avue-form v-model="form" :option="option"></avue-form>
<script>
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{},
        option: {
          column: [{
            label: '级联',
            prop: 'cascader',
            type: "cascader",
            dicData: dic
          }]
      }
    }
  }
}
</script>

```
:::


## 多选

:::demo `multiple`为`true`在开启多选模式后，默认情况下会展示所有已选中的选项的Tag，你可以使用`tags`为`true`来折叠Tag
```html
<avue-form v-model="form" :option="option"></avue-form>
<script>
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{
          cascader: [ [ "zhinan", "shejiyuanze", "yizhi" ], [ "zhinan", "shejiyuanze", "fankui" ] ]
        },
        option: {
          column: [{
            label: '级联',
            prop: 'cascader',
            type: "cascader",
            dicData: dic,
            multiple: true
          },{
            label: '级联',
            prop: 'cascader',
            type: "cascader",
            dicData: dic,
            tags:true,
            multiple: true
          }]
      }
    }
  }
}
</script>

```
:::

## 选中单节点

:::demo 
```html
<avue-form v-model="form" :option="option"></avue-form>
<script>
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{
          cascader:'yizhi'
        },
        option: {
          column: [{
            label: '级联',
            prop: 'cascader',
            type: "cascader",
            emitPath:false,
            dicData: dic
          }]
      }
    }
  }
}
</script>

```
:::

## 任意一级

:::demo 可通过`checkStrictly` 为`true` 来设置父子节点取消选中关联，从而达到选择任意一级选项的目的。
```html
<avue-form v-model="form" :option="option"></avue-form>
<script>
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{},
        option: {
          column: [{
            label: '级联',
            prop: 'cascader',
            type: "cascader",
            dicData: dic,
            checkStrictly: true
          }]
      }
    }
  }
}
</script>

```
:::


## 搜索

:::demo 将`filterable`赋值为true即可打开搜索功能，默认会匹配节点的label或所有父节点的label(由`showAllLevels`决定)中包含输入值的选项
```html
<avue-form v-model="form" :option="option"></avue-form>
<script>
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{},
        option: {
          column: [ {
            label: '级联',
            prop: 'cascader',
            type: "cascader",
            dicData: dic,
            filterable: true
          }]
      }
    }
  }
}
</script>

```
:::


## 自定义节点内容

:::demo 可以通过`typeslot`对级联选择器的备选项的节点内容进行自定义，scoped slot会传入两个字段 node 和 data，分别表示当前节点的 Node 对象和数据。
```html 
<avue-form v-model="form" :option="option">
  <template slot="cascaderType" slot-scope="{node,data}">
    <span>{{ (data || {}).label }}</span>
    <span v-if="!node.isLeaf"> ({{((data || {}).children || []).length }}) </span>
  </template>
</avue-form>
<script>
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{},
        option: {
          column: [{
            label: '级联',
            prop: 'cascader',
            type: "cascader",
            dicData: dic,
            typeslot: true
          }]
      }
    }
  }
}
</script>

```
:::

## 懒加载

:::demo 
```html
<avue-form v-model="form" :option="option"></avue-form>
<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
const dic=[{
  value: 'zhinan',
  label: '指南',
  children: [{
    value: 'shejiyuanze',
    label: '设计原则',
    children: [{
      value: 'yizhi',
      label: '一致'
    }, {
      value: 'fankui',
      label: '反馈'
    }]
  }]
}]
export default {
    data() {
      return {
        form:{
          cascader: ["110000", "110100", "110101"]
        },
        option: {
          column: [{
            label: '级联',
            prop: 'cascader',
            type: "cascader",
            props: {
              label: 'name',
              value: 'code'
            },
            lazy: true,
            lazyLoad(node, resolve) {
              let stop_level = 2;
              let level = node.level;
              let data = node.data || {}
              let code = data.code;
              let list = [];
              let callback = () => {
                resolve((list || []).map(ele => {
                  return Object.assign(ele, {
                    leaf: level >= stop_level
                  })
                }));
              }
              if (level == 0) {
                axios.get(`${baseUrl}/getProvince`).then(res => {
                  list = res.data.data;
                  callback()
                })
              }
              if (level == 1) {
                axios.get(`${baseUrl}/getCity/${code}`).then(res => {
                  list = res.data.data;
                  callback()
                })
              } else if (level == 2) {
                axios.get(`${baseUrl}/getArea/${code}`).then(res => {
                  list = res.data.data;
                  callback()
                })
              }else{
                 callback()
              }
            }
          }]
      }
    }
  }
}
</script>

```
:::