# 坐标选择器

```html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script type="text/javascript" src='https://webapi.amap.com/maps?v=1.4.11&key=7ab53b28352e55dc5754699add0ad862&plugin=AMap.PlaceSearch'></script>
<script src="https://webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
```

## 普通用法 
:::demo  
```html
<avue-form :option="option" v-model="form" ></avue-form>

<script>
export default{
  data() {
    return {
      form: {
        map: [ 113.10235504165291, 41.03624227495205, "内蒙古自治区乌兰察布市集宁区新体路街道顺达源广告传媒" ] 
      },
      option: {
          column: [
            {
              label: '坐标',
              prop: 'map',
              type: 'map',
              //高德初始化参数
              params:{
                zoom: 10,
                zoomEnable: false,
                dragEnable: false,
              }
            }]
        }
    }
  }
}
</script>

```
:::
