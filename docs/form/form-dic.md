# 数据字典

## 普通用法 
可以改变发送请求的方式和传入相关参数，目前只有get / post俩种，默认为get

:::demo  `dicMethod`为请求方式，`dicQuery`为需要传入的参数对象,请求看浏览器network
```html
<avue-form :option="option" v-model="form" ></avue-form>
<script>
let baseUrl = 'https://cli.avuejs.com/api/area';
export default {
  data(){
    return {
       form:{
          text:'',
       },
       option:{
          column: [{
            label: '本地字典',
            prop: 'text',
            type:'select',
            props: {
              label: 'name',
              value: 'code'
            },
            dicData:[{
              name:'本地字典1',
              code:0
            },{
              name:'本地字典2',
              code:1
            }],
          },{
            label: 'Get字典',
            prop: 'text1',
            props: {
              label: 'name',
              value: 'code'
            },
            dicUrl: `${baseUrl}/getProvince`,
            dicQuery:{
              a:1
            },
            type:'select'
          },{
            label: 'Post字典',
            prop: 'text2',
            props: {
              label: 'name',
              value: 'code'
            },
            dicUrl: `${baseUrl}/getProvince`,
            dicMethod:'post',
            dicQuery:{
              a:1
            },
            type:'select'
          }]
       }
    }
  }
}
</script>

```
:::


## 查找列的配置
:::demo  调用内置方法`findObject`查找对应`prop`的属性序号 ,同时你也可以更新字典
```html
<el-button  type="primary" size="small" @click="updateOption">点我更改配置</el-button><br /><br />
<avue-form ref="form" :option="option" v-model="form"></avue-form>
<script>
export default {
  data(){
    return {
        form:{},
        option:{
          column: [{
            label: '下拉',
            prop:'select',
            span:24,
            type:'select',
          }]
        }
    }
  },
  methods:{
    updateOption(){
       var column = this.findObject(this.option.column,'select');
       column={
         type:'radio',
         label:'单选框',
         dicData:[{
          label:'字典1',
          value:1
          },{
            label:'字典0',
            value:2
          }]
       }
    }
  }
}
</script>

```
:::



## 更新字典
:::demo  和上面方法一样，只是再调用`updateDic`时不需要传新的字典，他会自己调用`dicUrl`去请求字典
```html
<el-button  type="primary" size="small" @click="updateUrlDic">点我更新字典</el-button><br /><br />
<avue-form ref="form2" :option="option2" v-model="form"></avue-form>
<script>
export default {
  data(){
    return {
        form:{},
        option2:{
          column: [{
            label: '字典',
            span:24,
            type:'radio',
            prop: 'radio',
            dicUrl: 'https://cli.avuejs.com/api/area/getProvince',
            props: {
              label: 'name',
              value: 'code'
            }
          }]
        }
    }
  },
  methods:{
    updateUrlDic(){
       var form = this.$refs.form2
       this.$message.success('先设置本地字典1s后请求url')
       form.updateDic('radio',[{
          name:'字典1',
          code:1
        },{
          name:'字典0',
          code:2
        }]);
        setTimeout(()=>{
          form.updateDic('radio');
        },1000)
    },
  }
}
</script>

```
:::







