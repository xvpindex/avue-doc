# 组件对象
:::tip
 2.6.2+
::::

## 普通用法

:::demo  
```html
<avue-form ref="form" v-model="form" :option="option"></avue-form>

<script>
export default {
    data() {
      return {
        form:{},
        option:{
          labelWidth: 120,
          column: [{
              label: '测试框',
              prop: 'text',
          }]
        }
      };
    },
    mounted(){
      this.$message.success('查看控制台');
      console.log('text的ref对象')
      console.log(this.$refs.form.getPropRef('text'))
    }
}
</script>
```
:::