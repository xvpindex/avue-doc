# 详情编辑

:::tip
 2.5.2+
::::



:::demo  `detail`控制是否为详情页,单独也可以给某一列配置
```html
  <el-button @click="handle" style="margin-left: 20px">{{title}}</el-button>
  <br /><br />
  <avue-form  :option="option" v-model="obj" @submit="submit"></avue-form>
<script>
export default {
    computed: {
      title() {
        return this.option.detail ? '编 辑' : '保 存'
      }
    },
    data() {
      return {
        type: 0,
        obj: {},
        option: {
          detail:true,
          labelWidth:110,
          group: [
            {
              label: '用户信息',
              prop: 'jbxx',
              icon: 'el-icon-edit-outline',
              column: [
                {
                  label: '姓名',
                  prop: 'name',
                  rules: [
                    {
                      required: true,
                      message: '请输入姓名',
                      trigger: 'blur'
                    }
                  ]
                },
                {
                  label: '性别',
                  prop: 'sex',
                }
              ]
            }, {
              label: '退款申请',
              prop: 'tksq',
              icon: 'el-icon-view',
              column: [
                {
                  label: '省份',
                  span:24,
                  prop: 'province',
                  type: 'select',
                  props: {
                    label: 'name',
                    value: 'code'
                  },
                  dicUrl: `https://cli.avuejs.com/api/area/getProvince`,
                  rules: [
                    {
                      required: true,
                      message: '请选择省份',
                      trigger: 'blur'
                    }
                  ]
                },
                {
                  label: '多选',
                  prop: 'checkbox',
                  type: 'checkbox',
                  all:true,
                  props: {
                    label: 'name',
                    value: 'code'
                  },
                  span: 24,
                  dicUrl: 'https://cli.avuejs.com/api/area/getProvince'
                }
              ]
            }
            , {
              label: '用户信息',
              prop: 'yhxx',
              icon: 'el-icon-edit-outline',
              column: [
                {
                  label: '测试长度',
                  prop: 'len',
                  value:3,
                  maxlength: 5,
                }, {
                  label: '测试自定义',
                  prop: 'lens',
                  value:3,
                  formslot: true
                }
              ]
            }
          ]
        }
      }
    },
    mounted() {
      setTimeout(() => {
        this.obj = {
          name: 'small',
          province: '110000',
          checkbox: ['110000']
        }
      }, 100)
    },
    methods: {
      handle() {
       this.option.detail=!this.option.detail
      },
      submit() {
        this.$message.success(JSON.stringify(this.obj))
      }
    }
}
</script>
```
:::



:::demo  
```html
<el-button @click="detail=!detail">{{title}}</el-button>
<br /><br />
<avue-form :option="option" v-model="obj" @submit="submit">
  <template slot="datetime" slot-scope="scope" >
    <span v-if="detail">
      这是我要选择的日期{{datetime[0]}}年{{datetime[1]}}月{{datetime[2]}}日
    </span>
    <el-input v-else v-model="obj.datetime"></el-input>
  </template>
</avue-form>
<script>
export default {
  computed: {
      datetime() {
        return this.obj.datetime.split('-')
      },
      option() {
        return {
          detail: this.detail,
          column: [{
            label: '选择日期',
            span: 12,
            prop: 'datetime',
            type: 'datetime',
            format: "yyyy-MM-dd",
            valueFormat: "yyyy-MM-dd"
          },{
            label: '',
            labelWidth: 10,
            prop: 'divider',
            display: !this.detail,
            component: 'elDivider',//ele分割线
            span: 24,
            params: {
              html: '这是一大堆的文字介绍，很长 很长 很长成这是一大堆的文字介绍，很长 很长 很长成这是一大堆的文字介绍，很长 很长 很长成',
              contentPosition: "left",
            }
          }]
        }
      },
      title() {
        return this.detail ? '编辑' : '保存'
      },
    },
    data() {
      return {
        detail: true,
        obj: {
          datetime: '2020-05-01'
        }
      }
    },
    methods: {
      submit() {
        this.$message.success(JSON.stringify(this.obj))
      }
    }
}
</script>
```
:::


