# 自定义卡槽


## 自定义内容

:::demo  
```html
<avue-form v-model="form" :option="option">
  <template slot-scope="scope" slot="text">
    <div>
      <el-tag>{{form.text?form.text:'暂时没有内容'}}</el-tag>
      <el-input v-model="form.text" placeholder="这里是自定的表单"></el-input>
    </div>
  </template>
</avue-form>

<script>
export default {
    data() {
      return {
        form:{
          text:''
        },
        option:{
          labelWidth: 120,
          column: [{
              label: '姓名',
              prop: 'text',
              formslot:true,
              rules: [{
                required: true,
                message: "请输入姓名",
                trigger: "blur"
              }]
          }]
        }
      };
    }
}
</script>
```
:::


## 自定义标题

:::demo  
```html
<avue-form v-model="form" :option="option">
   <template slot-scope="{}" slot="textLabel">
     <span>姓名&nbsp;&nbsp;</span>
     <el-tooltip class="item" effect="dark" content="文字提示" placement="top-start">
      <i class="el-icon-warning"></i>
    </el-tooltip>
  </template>
</avue-form>

<script>
export default {
    data() {
      return {
        form:{
          text:''
        },
        option:{
          labelWidth: 120,
          column: [{
              label: '姓名',
              prop: 'text',
              labelslot:true,
              rules: [{
                required: true,
                message: "请输入姓名",
                trigger: "blur"
              }]
          }]
        }
      };
    }
}
</script>
```
:::

## 自定义错误提示

:::demo  
```html
<avue-form v-model="form" :option="option">
   <template slot-scope="{error}" slot="textError">
      <p style="color:green">自定义提示{{error}}</p>
  </template>
</avue-form>

<script>
export default {
    data() {
      return {
        form:{
          text:''
        },
        option:{
          labelWidth: 120,
          column: [{
              label: '姓名',
              prop: 'text',
              errorslot:true,
              rules: [{
                required: true,
                message: "请输入姓名",
                trigger: "blur"
              }]
          }]
        }
      };
    }
}
</script>
```
:::


## 自定义按钮

:::demo  
```html
<avue-form ref="form" v-model="form" :option="option">
  <template slot="menuForm">
    <el-button  icon="el-icon-user" type="primary" @click="handleSubmit">提 交</el-button>
    <el-button  icon="el-icon-delete" @click="handleEmpty">清 空</el-button>
  </template>
</avue-form>

<script>
export default {
    data() {
      return {
        form:{},
        option:{
          emptyBtn:false,
          submitBtn:false,
          column: [{
            label: "用户名",
            prop: "username",
            rules: [{
                required: true,
                message: "请输入用户名",
                trigger: "blur"
            }]
          }]
        }
      }
    },
    methods:{
      handleEmpty(){
        this.$refs.form.resetForm();
      },
      handleSubmit(){
        //this.$refs.form.submit();
        this.$refs.form.validate((vaild,done)=>{
          if(vaild){
            this.$message.success(JSON.stringify(this.form));
            setTimeout(()=>{
              done()
            },2000)
          }
        })
      }
    }
  }
</script>
```
:::


## 自定义样式
:::tip
 2.7.1+
::::

```css
.formClassName{
  padding: 0 0 0 55px !important;
  background-color: #409eff;
  color: #fff;
}
```

:::demo  `className`属性配置上样式的名字即可
```html
<avue-form :option="option" v-model="form" > </avue-form>

<script>
export default{
  data() {
    return {
      form: { },
      option: {
        column: [
          {
            label:'姓名',
            prop:'name'
          },
          {
            label: '性别',
            prop: 'sex',
            labelPosition:'top',
            span:24,
            className:'formClassName'
          }
        ]
      }
    }
  }
}
</script>

```
:::
