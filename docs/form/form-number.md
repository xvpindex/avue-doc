
# 数字输入框



## 普通用法

:::demo
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {
        num:1
      },
      option: {
        column: [
         {
            label:'数字输入框',
            prop:'num',
            type:'number'
         }
        ]
      }
    }
  }
}
</script>

```
:::


## 隐藏控制器

:::demo 设置`controls`属性可以隐藏控制器，接受一个`Number`。
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {
        num:1
      },
      option: {
        column: [{
            label:'数字输入框',
            prop:'num',
            controls:false,
            type:'number'
         }
        ]
      }
    }
  }
}
</script>


```
:::

## 步数

:::demo 设置`step`属性可以控制步长，接受一个`Number`。
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {
        num:1
      },
      option: {
        column: [
         {
            label:'数字输入框',
            prop:'num',
            step:2,
            type:'number'
         }
        ]
      }
    }
  }
}
</script>

```
:::


## 严格步数

:::demo `stepStrictly`属性接受一个`Boolean`。如果这个属性被设置为`true`，则只能输入步数的倍数。
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {
        num:1
      },
      option: {
        column: [
         {
            label:'数字输入框',
            prop:'num',
            step:2,
            stepStrictly:true,
            type:'number'
         }
        ]
      }
    }
  }
}
</script>

```
:::


## 精度

:::demo 设置`precision`属性可以控制数值精度，接收一个`Number`。
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {
        num:1
      },
      option: {
        column: [
         {
            label:'数字输入框',
            prop:'num',
            precision:2,
            type:'number'
         }
        ]
      }
    }
  }
}
</script>

```
:::


## 按钮位置

:::demo 设置 `controlsPosition`属性可以控制按钮位置。
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {
        num:1
      },
      option: {
        column: [
         {
            label:'数字输入框',
            prop:'num',
            controlsPosition:'',
            type:'number'
         }
        ]
      }
    }
  }
}
</script>

```
:::








