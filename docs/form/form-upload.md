# 附件上传

## 普通用法
- 如果你想修改file文件,由于上传的file是只读文件，
- 必须复制新的file才可以修改名字，完后赋值到done函数里
- 如果不修改的话直接写done()即可,可以参考uploadBefore函数
- 阿里云和七牛云同理使用
- 你也可以直接单独配置到column中，例如
```js
{
  type:'upload',
  uploadBefore:()=>{},
  uploadAfter:()=>{}
}
```


:::demo  提供了`dataType`参数可以去配置存到数据库的结构体类型,可以配置`canvasOption`属性去生成水印和压缩图片，也可配`props`和`propsHttp`属性去加载不用的结构体中的`key`和`value`,具体参数配置参考下面demo和文档
```html
<avue-form :option="option" v-model="form" :upload-preview="uploadPreview" 
:upload-error="uploadError" :upload-exceed="uploadExceed" :upload-delete="uploadDelete" :upload-before="uploadBefore" :upload-after="uploadAfter"> 
  <template slot="imgUrlType" slot-scope="{file}">
    <span>自定义卡槽{{file}}</span>
  </template>
</avue-form>

<script>
export default{
  data() {
    return {
      form: {
          video:'/i/movie.ogg',
          imgUrl:[
            { "label": "avue@226d5c1a_image.png", "value": "/images/logo-bg.jpg" },
            {"label": "avue@226d5c1a_video.png", "value": 'https://www.w3school.com.cn/i/movie.ogg'}
          ],
          imgUrl5:[
            { "label": "avue@226d5c1a_image.png", "value": "/images/logo-bg.jpg" },
            {"label": "avue@226d5c1a_video.png", "value": 'https://www.w3school.com.cn/i/movie.ogg'}
          ],
          imgUrl3:'/images/logo-bg.jpg',
          string:'/images/logo-bg.jpg,/images/logo-bg.jpg',
          img:['/images/logo-bg.jpg','/images/logo-bg.jpg','./xx/xx.sql']
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '视频',
            prop: 'video',
            type: 'upload',
            accept:"video/mp4",
            propsHttp: {
              home: 'https://www.w3school.com.cn'
            },
            listType: 'picture-img',
            span: 24,
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '附件上传',
            prop: 'imgUrl',
            type: 'upload',
            loadText: '附件上传中，请稍等',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '水印头像',
            prop: 'imgUrl3',
            type: 'upload',
            listType: 'picture-img',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            canvasOption: {
              text: 'avue',
              ratio: 0.1
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '照片墙',
            prop: 'imgUrl4',
            type: 'upload',
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            propsHttp: {
              res: 'data'
            },
            action: '/imgupload'
          },
          {
            label: '数组图片组',
            prop: 'img',
            dataType: 'array',
            type: 'upload',
            propsHttp: {
              res: 'data.0'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '字符串图片组',
            prop: 'string',
            dataType: 'string',
            type: 'upload',
            propsHttp: {
              res: 'data'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '拖拽上传',
            prop: 'imgUrl5',
            type: 'upload',
            span: 24,
            drag: true,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '缩略图上传',
            prop: 'imgUrl6',
            type: 'upload',
            limit: 3,
            span: 24,
            propsHttp: {
              res: 'data'
            },
            listType: 'picture',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  },
  methods: {
    uploadDelete(column,file) {
      console.log(column,file)
      return this.$confirm(`这里是自定义的，是否确定移除该选项？`);
    },
    uploadBefore(file, done, loading,column) {
      console.log(file,column)
      //如果你想修改file文件,由于上传的file是只读文件，必须复制新的file才可以修改名字，完后赋值到done函数里,如果不修改的话直接写done()即可
      var newFile = new File([file], '1234', { type: file.type });
      done(newFile)
      this.$message.success('上传前的方法')
    },
    uploadError(error, column) {
      this.$message.success('上传失败')
      console.log(error, column)
    },
    uploadAfter(res, done, loading,column) {
      console.log(res,column)
      done()
      this.$message.success('上传后的方法')
    },
    uploadPreview(file,column,done){
      console.log(file,column)
      done()//默认执行打开方法
      this.$message.success('自定义查看方法,查看控制台')
    },
    uploadExceed(limit, files, fileList, column){
      console.log(limit, files, fileList, column)
      this.$message.success('自定义查看方法,查看控制台')
    },
    submit() {
      this.$message.success('当前数据' + JSON.stringify(this.form))
    }
  }
}
</script>


```
:::


## 阿里云oss上传



```html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://avuejs.com/cdn/aliyun-oss-sdk.min.js"></script>
```
```js
//入口处全局配置阿里云的参数
Vue.use(AVUE, {
  ali: {
    region: 'oss-cn-beijing',
    endpoint: 'oss-cn-beijing.aliyuncs.com',
    stsToken:'',
    accessKeyId: '',
    accessKeySecret: '',
    bucket: 'avue',
  }
})
```

:::tip
 1.0.6+
::::


:::demo 
```html
<avue-form :option="option" v-model="form"> </avue-form>
<script>
export default {
   data(){
     return {
       form:{},
       option: {
          labelWidth: 120,
          column: [
            {
              label: '阿里上传',
              prop: 'imgUrl',
              type: 'upload',
              listType: 'picture-img',
              canvasOption: {
                text: 'avue',
                ratio: 0.1
              },
              oss: 'ali',
              loadText: '附件上传中，请稍等',
              span: 24,
              tip: '只能上传jpg/png文件，且不超过500kb',
            }
          ]
        }
     }
   }
}
</script>

```
:::



## 七牛云oss上传



```html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://avuejs.com/cdn/CryptoJS.js"></script>
```
- 华东——http(s)://upload.qiniup.com
- 华北——http(s)://upload-z1.qiniup.com
- 华南——http(s)://upload-z2.qiniup.com
- 北美——http(s)://upload-na0.qiniup.com
- 东南亚——http(s)://upload-as0.qiniup.com

```js
//入口处全局配置七牛云的参数 
Vue.use(window.AVUE, {
  qiniu: {
    AK: '',//七牛云相关密钥
    SK: '',//七牛云相关密钥
    bucket:'https://upload.qiniup.com'//存储地区，默认为华东，其他的如下表
    scope: 'test',//存储空间名称
    url: 'https://cdn.avuejs.com/'//外链的域名地址
  }
})
```

:::tip
 1.0.6+
::::


:::demo 
```html
<avue-form :option="option" v-model="form"> </avue-form>
<script>
export default {
   data(){
     return {
       form:{},
       option: {
          labelWidth: 120,
          column: [
            {
              label: '七牛上传',
              prop: 'imgUrl',
              type: 'upload',
              listType: 'picture-img',
              propsHttp: {
                name: 'hash',
                url: "key"
              },
              oss: 'qiniu',
              loadText: '附件上传中，请稍等',
              span: 24,
              tip: '只能上传jpg/png文件，且不超过500kb',
            }
          ]
        }
     }
   }
}
</script>

```
:::


