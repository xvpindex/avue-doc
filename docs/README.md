---
home: true
heroImage: /images/logo.png
actionText: 开始学习(v2.8.22)
actionLink: /docs/installation
features:
- title: 易用灵活
  details: 已经会了 Vue Element-ui？即刻阅读文档开始使用吧。
- title: 丰富组件
  details: 包含了大量的常用组件库以及插件库。
- title: 高效兼容
  details: 兼容现在主流的浏览器，开箱即用的插件引入模式。
footer: MIT Licensed | Copyright © 2018-present Smallwei 网站备案号：蒙ICP备[19003764号-1]
---

### 快速上手

``` bash
# 安装
yarn add  @smallwei/avue -S # 或者：npm i @smallwei/avue -S

# 引入
import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
Vue.use(Avue);

```

### 问题反馈
[github问题反馈](https://github.com/nmxiaowei/avue/issues)  
[gitee问题反馈](https://gitee.com/smallweigit/avue/issues) 

### 交流群
- QQ交流群1：606410437
- QQ交流群2：387300192
- QQ交流群3：535598801


### 友情链接

[Pig](https://pig4cloud.com/#/)
[Bladex](https://bladex.vip/#/)
[如梦技术](https://www.dreamlu.net/)
[XUNHUAN](https://xunhuan.me/)
[开源运营离黍](https://clenji.com/)
[ouxiang](https://ouxiang.me/)
[Guns](https://www.stylefeng.cn/)
[个人支付接口](https://www.yungouos.com/#/invite/welcome?spm=MTA0MTc=)
[Jwchat](https://codegi.gitee.io/jwchatdoc/)
[JeeSite](https://jeesite.com/)
[全栈之巅](https://space.bilibili.com/341919508)
