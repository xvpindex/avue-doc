# 打印插件

## 代码
[代码地址](https://gitee.com/smallweigit/avue-plugin-print)



## 使用
- 1.npm install avue-plugin-print --save
- 2.import $Print from 'avue-plugin-print'
- 3.$Print(el);
- 4.参考如下代码使用


<Print></Print>
```html
<template>
  <div id="all">
    <h2 style="color:red"
        id="test">我是标题1</h2>
    <h2 style="color:green">我是标题2</h2>
    <h2 class="no-print">我会被忽略</h2>
    <button @click="handlePrint">打印全部</button>&nbsp;&nbsp;
    <button @click="handlePrint1">打印局部</button>
  </div>
</template>

<script>
import $Print from 'avue-plugin-print'
export default {
  methods: {
    $Print,
    handlePrint () {
      this.$Print('#all')
    },
    handlePrint1 () {
      this.$Print('#test')
    }
  }
}
</script>
```
