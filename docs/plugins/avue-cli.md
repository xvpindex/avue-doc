# cli后台模板


## 在线体验
[在线体验](https://cli.avuejs.com)  

## 代码地址
[代码地址](https://gitee.com/smallweigit/avue-cli)


## 在线文档
[文档地址](https://www.kancloud.cn/smallwei/avue)
- 为保证文档实时更新及其多终端阅读，已托管[看云](https://www.kancloud.cn/smallwei/avue)
- 关于文档价格，**全部章节 59.99元**，关键部署章节、Q&A 等已免费提供给大家
- 关于文档收费问题，**即使没有此文档，我相信大多数同学都能运行起来avue-cli**，文档中书写花费了大量时间，也是对我们团队的支持。

## 功能
- 登录/注销
  - 用户名登录
  - 验证码登录
  - 第三方登陆(QQ,微信)
  - 人脸识别登录
- 错误的日志记录
- 灵活的10+多款主题自由配置
- 路由权限、菜单权限、登录权限
- 本地化持久存储api
- 页面缓冲
- 面向全屏幕尺寸的响应式适配能力
- 对国际化的支持
- 自动刷新token等机制
- 全新的前端错误日志监控机制
- 前端路由动态服务端加载
- 无限极动态路由加载
- 模块的可拆卸化,达到开箱即用
- 更多。。。

## 页面展示
### 登陆
<img src='/images/module1.jpg' width="700">

### 主页
<img src='/images/module2.jpg' width="700">

### 炫酷主题
<img src='/images/module3.jpg' width="700">

### MAC主题
<img src='/images/module12.jpg' width="700">
<img src='/images/module13.jpg' width="700">

### 日志监控
<img src='/images/module4.jpg' width="700">

### 错误提示
<img src='/images/module5.jpg' width="700">

### 数据展示
<img src='/images/module6.jpg' width="700">

### 第三方网站
<img src='/images/module7.jpg' width="700">

### 全局搜索
<img src='/images/module9.jpg' width="700">

### 个人中心
<img src='/images/module10.jpg' width="700">

### 个人设置
<img src='/images/module11.jpg' width="700">

