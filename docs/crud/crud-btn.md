# 自定义按钮
点击编辑或新增去体验
:::tip
 2.0.6+
::::

:::demo  按钮的显隐全部接受`Boolean`类型的值;`columnBtn`为列动态显隐按钮;`refreshBtn`刷新按钮;`addBtn`表格新增按钮;`editBtn`行编辑按钮;`delBtn`行删除按钮;自定义按钮调用组件中对应方法即可,例如编辑方法`rowEdit`
```html
<avue-crud :data="data" :option="option" ref="crud" @row-save="handleSave" @row-update="handleUpdate">
  <template slot-scope="scope" slot="menuLeft">
    <el-button type="danger"
      icon="el-icon-plus"
      size="small"
      plain
      @click.stop="$refs.crud.rowAdd()">新增</el-button>
  </template>
   <template slot-scope="{row,index,type}" slot="menuForm">
    <el-button type="primary"
                     icon="el-icon-check"
                     size="small"
                     plain
                     v-if="type=='add'"
                     @click="$refs.crud.rowSave()"
                    >自定义新增</el-button>
    <el-button type="primary"
                     icon="el-icon-check"
                     size="small"
                     plain
                    v-if="type=='edit'"
                     @click="$refs.crud.rowUpdate()"
                    >自定义修改</el-button>
   <el-button type="primary"
                     icon="el-icon-check"
                     size="small"
                     plain
                     @click="$refs.crud.closeDialog()"
                    >取消</el-button>
  </template>
  <template slot-scope="{row,index}" slot="menu">
     <el-button type="primary"
                     icon="el-icon-check"
                     size="small"
                     plain
                     @click.stop="$refs.crud.rowEdit(row,index)">编辑</el-button>
      <el-button type="success"
                     icon="el-icon-check"
                     size="small"
                     plain
                     @click.stop="$refs.crud.rowView(row,index)">查看</el-button>
  </template>
</avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          columnBtn:false,
          refreshBtn:false,
          saveBtn:false,
          updateBtn:false,
          cancelBtn:false,
          addBtn:false,
          delBtn:false,
          editBtn:false,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods:{
      handleSave(form,done,loading){
        this.$message.success('新增数据'+ JSON.stringify(form));
        done();
      },
      handleUpdate(form,index,done,loading){
        this.$message.success('编辑数据'+ JSON.stringify(form)+'数据序号'+index);
        done();
      }
    }
}
</script>
```
:::