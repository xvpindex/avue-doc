# 列显隐
:::tip
 2.5.0+
::::

## 普通用法
:::demo 一共是有4列的`hide`和`showColumn`可以达到控制列显隐控制,也可以持久化存储哦
```html
<avue-crud :option="option" :data="data" ></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2',
          text3:'内容1-3',
          text4:'内容1-4'
       },{
          text1:'内容2-1',
          text2:'内容2-2',
          text3:'内容2-3',
          text4:'内容2-4'
       }],
       option:{
          align:'center',
          headerAlign:'center',
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }, {
            label: '列内容3',
            prop: 'text3',
            hide:true
          }, {
            label: '列内容4',
            prop: 'text4',
            hide:true,
            showColumn:false,
          }]
       }
    }
  },
  methods:{
   
  }
}
</script>

```
:::

