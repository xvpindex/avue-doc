# 自定义表单
:::tip
 1.0.0+
::::

## 普通用法

:::demo 设置列的属性`formslot`为`true`时即可开启表单自定义模式，在卡槽中指定列的名字加上Form即可 
```html
<avue-crud :data="data" :option="option" v-model="user">
  <template slot-scope="{type,disabled}" slot="nameForm">
      <el-tag>窗口类型:{{type=='add'?'新增':'修改'}}</el-tag>
      <el-tag>{{user.name?user.name:'暂时没有内容'}}</el-tag>
      <el-input :disabled="disabled" v-model="user.name"></el-input>
  </template>
   <template slot-scope="scope" slot="nameLabel">
     <span>姓名&nbsp;&nbsp;</span>
     <el-tooltip class="item" effect="dark" content="文字提示" placement="top-start">
      <i class="el-icon-warning"></i>
    </el-tooltip>
  </template>
   <template slot-scope="{error}" slot="nameError">
      <p style="color:green">自定义提示{{error}}</p>
  </template>
</avue-crud>

<script>
export default {
 data() {
      return {
        user:{},
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          viewBtn:true,
          column:[
             {
              label:'姓名',
              prop:'name',
              formslot:true,
              labelslot:true,
              errorslot:true,
              rules: [{
                required: true,
                message: "请输入姓名",
                trigger: "blur"
              }]
            },
            {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
  }
</script>
```
:::