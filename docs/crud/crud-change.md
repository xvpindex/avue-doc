# 多表格切换
:::tip
 2.0.5+
::::



:::demo 动态改变`option`配置即可
```html
<avue-crud ref="crud" :data="data" :option="option" @row-click="handleRowClick">
  <template slot="menuLeft">
    <el-button type="primary" size="small" @click="handleSwitch" icon="el-icon-sort">切 换</el-button>
  </template>
</avue-crud>

<script>
export default {
    data() {
      return {
        type:true,
        data: [
          {
            name:'张三',
            sex:'男',
            username:'smallwei',
            password:'smallwei'
          }, {
            name:'李四',
            sex:'女',
            username:'avue',
            password:'avue'
          }
        ],
        option:{},
        option1:{
          addBtn:false,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
        option2:{
          addBtn:false,
          column:[
             {
              label:'用户名',
              prop:'username',
              search:true
            }, {
              label:'密码',
              prop:'password',
              type:'password',
              search:true
            }, {
              label:'姓名',
              prop:'name',
              search:true
            }
          ]
        },
      };
    },
    mounted(){
      this.handleSwitch();
    },
    methods: {
      handleSwitch(){
        this.type=!this.type;
        if(this.type){
          this.option=this.option1;
        }else{
           this.option=this.option2;
        }
        this.$nextTick(()=>{
          this.$refs.crud.columnInit();
        })
      }
    }
}
</script>
```
:::
