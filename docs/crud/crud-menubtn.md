# 弹出表单按钮位置
点击新增进行体验，同时可以切换按钮组的位置
:::tip
 2.3.3+
::::


:::demo 配置`dialogMenuPosition`属性值即可，默认为`right`
```html
<el-radio-group v-model="direction">
  <el-radio label="left">居左</el-radio>
  <el-radio label="center">居中</el-radio>
  <el-radio label="right">居右</el-radio>
</el-radio-group>
<br/><br/>
<avue-crud :option="option" :data="list"></avue-crud>
<script>
export default {
  watch:{
    direction(value){
      this.option.dialogMenuPosition=value;
    }
  },
  data(){
    return {
      direction:'right',
      list:[{
        id:1,
        name:'张三',
        sex:12
      },{
        id:2,
        name:'李四',
        sex:12
      }],
      option:{
        dialogMenuPosition:'right',
        column:[{
          label:'id',
          prop:'id'
        },{
          label:'姓名',
          prop:'name'
        },{
          label:'年龄',
          prop:'sex'
        }]
      }
    }
  }
}
</script>

```
:::

