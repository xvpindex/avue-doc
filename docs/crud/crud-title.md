# 标题

:::tip
 2.8.0+
::::

## 普通用法

:::demo  可以配置表格`title`的相关属性
```html
<avue-crud :data="data" :option="option"></avue-crud>

<script>
export default {
 data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }, {
            name:'王五',
            sex:'女'
          }, {
            name:'赵六',
            sex:'男'
          }
        ],
        option:{
          title:'表格的标题',
          titleSize:'h3',
          titleStyle:{
            color:'red'
          },
          column:[
             {
              label:'姓名',
              prop:'name'
            },
            {
              label:'性别',
              prop:'sex'
            }
          ]
        }
      }
    }
  }
</script>
``` 
:::
