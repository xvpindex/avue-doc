# 组件对象
:::tip
 2.6.2+
::::

## 普通用法

:::demo  点击新增或者编辑
```html
<avue-crud ref="crud" :data="data" :before-open="beforeOpen" :option="option"></avue-crud>

<script>
export default {
    data() {
      return {
        data:[{
          text:'测试数据'
        }],
        option:{
          labelWidth: 120,
          column: [{
              label: '测试框',
              prop: 'text',
          }]
        }
      };
    },
    methods:{
      beforeOpen(done){
        done()
         setTimeout(()=>{
          this.$message.success('查看控制台');
          console.log('text的ref对象')
          console.log(this.$refs.crud.getPropRef('text'))
        },0)
      }
    }
}
</script>
```
:::