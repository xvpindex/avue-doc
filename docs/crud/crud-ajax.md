# 服务端动态加载
:::tip
 2.3.0+
::::




:::demo
```html
<avue-crud ref="crud" :data="data"  :option="option" :table-loading="loading"></avue-crud>

<script>
export default {
    data() {
      return {
        loading:true,
        data: [],
        option:{
         
        },
      };
    },
    mounted(){
      this.$message.success('模拟2s后服务端动态加载');
      setTimeout(()=>{
        this.option={
          border:true,
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            },{
              label: '省份',
              prop: 'province',
              type: 'select',
              props: {
                label: 'name',
                value: 'code'
              },
              dicUrl: `https://cli.avuejs.com/api/area/getProvince`,
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            }]
        }
        this.data=[
          {
            name:'张三',
            sex:'男',
            province:'110000'
          }, {
            name:'李四',
            sex:'女',
            province:'120000'
          }
        ]
        this.$nextTick(()=>{
          this.loading=false;
          this.$refs.crud.init();
        })
       
      },2000)
    }
}
</script>

```
:::