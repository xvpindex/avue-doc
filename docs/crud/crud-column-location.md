# 列配置持久化
:::tip
 2.8.6+
::::

## 普通用法
:::demo 
```html
<avue-crud defaults.sync="defaults" ref="crud"  :option="option" :data="data">
  <template slot="menuLeft" slot-scope="{size}">
    <el-button @click="saveOption" type="danger" :size="size">保存配置</el-button>
  </template>
</avue-crud>
<script>
let key = 'avue-column';
export default {
  data(){
    return {
       defaults:{},
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       },{
          text1:'内容3-1',
          text2:'内容3-2'
       },{
          text1:'内容4-1',
          text2:'内容4-2'
       },{
          text1:'内容5-1',
          text2:'内容5-2'
       }],
       option:{
          sortable:true,
          addBtn:false,
          menu:false,
          border:true,
          align:'center',
          column: [{
            label: '列内容1',
            prop: 'text1',
            hide:false
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  },
  mounted(){
    this.$nextTick(()=>{
      this.defaults = JSON.parse(localStorage.getItem(key) || '')
    })
  },
  methods:{
    saveOption(){
      localStorage.setItem(key,JSON.stringify(this.defaults))
      this.$message.success('配置保存成功')
    }
  }
}
</script>

```
:::



