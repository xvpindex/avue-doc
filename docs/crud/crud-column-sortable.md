# 表格列拖拽排序
:::tip
 2.5.3+
::::
- 拖拽列的名字可以进行排序
```
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://cdn.staticfile.org/Sortable/1.10.0-rc2/Sortable.min.js"></script>
```

## 普通用法
:::demo 
```html
<avue-crud ref="crud"  :option="option" :data="data"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       },{
          text1:'内容3-1',
          text2:'内容3-2'
       },{
          text1:'内容4-1',
          text2:'内容4-2'
       },{
          text1:'内容5-1',
          text2:'内容5-2'
       }],
       option:{
          sortable:true,
          addBtn:false,
          menu:false,
          border:true,
          align:'center',
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  },
  mounted(){
    this.$refs.crud.$refs.dialogColumn.columnBox=true;
  },
}
</script>

```
:::




