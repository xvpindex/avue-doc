# 弹窗执行前后
:::tip
 1.0.0+
::::




:::demo 其实在这个方法里面可以干很多操作，看例子代码

```html
<avue-crud
  :data="data"
  v-model="form"
  :before-open="beforeOpen"
  :before-close="beforeClose"
  :option="option"
></avue-crud>

{{form}}

<script>
  export default {
    data() {
      return {
        form:{},
        data: [{
            name:'张三',
            sex:'男'
        }],
        option:{
          align:'center',
          menuAlign:'center',
          viewBtn:true,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
      beforeClose(done,type){
        this.$confirm('确认关闭？')
          .then(_ => {
            done();
          })
          .catch(_ => {});
        
      },
      beforeOpen(done,type){
        this.$alert(`我是${type}`, {
          confirmButtonText: '确定',
          callback: action => {
            if(['view','edit'].includes(type)){
              // 查看和编辑逻辑
            }else{
              //新增逻辑
              this.form.name='初始化赋值'
            }
            done();
          }
        });
          
      }
    }
  };
</script>
```
