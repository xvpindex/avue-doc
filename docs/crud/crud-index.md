# 索引
:::tip
 1.0.0+
::::

## 普通用法

:::demo  设`index`属性为`true`即可，`indexLabel`设置表格的序号的标题,默认为`#`
```html
<avue-crud :data="data" :option="option" ></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          index:true,
          indexLabel:'序号',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
    }
}
</script>
```
:::


## 自定义索引

:::demo 
```html
<avue-crud :data="data" :option="option" >
  <template slot="index" slot-scope="{row,index}">
    <el-tag>{{index+1}}</el-tag>
  </template>
</avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          column:[{
              label:'序号',
              prop:'index',
              fixed:true
            },
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
    }
}
</script>
```
:::