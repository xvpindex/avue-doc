# 展开行
指定行展开，或则默认全部展开
:::tip
 1.0.8+
::::


## 普通用法
:::demo  使用`expand`属性时必须配置`rowKey`属性为你行数据的主键，不能重复, `defaultExpandAll`属性默认展开全部,`expandRowKeys`为展开指定`rowKey`主键的数组，同时你也可以调用`toggleRowExpansion`方法传入你要展开的`row`
```html
<avue-crud ref="crud" :option="option" :data="data" @expand-change="expandChange">
  <template slot="expand" slot-scope="{row}">
    {{row}}
  </template>
</avue-crud>
<script>
export default {
 data() {
    return {
      option: {
        expand: true,
        expandRowKeys:[1],
        rowKey:'id',
        column: [{
          label: '姓名',
          prop: 'name'
        }, {
          label: '年龄',
          prop: 'sex'
        }]
      },
      data: [{
        id: 1,
        name: '张三',
        sex: 12,
      }, {
        id: 2,
        name: '李四',
        sex: 20,
      }]
    }
  }, methods: {
    expandChange(row, expendList) {
      this.$message.success('展开回调')
    },
  }
}
</script>

```
:::

## 手风琴模式
:::demo  `expand-change`配置`expandRowKeys`去使用
```html
<avue-crud ref="crud" :option="option1" :data="data" @expand-change="expandChanges">
  <template slot="expand" slot-scope="{row}">
    {{row}}
  </template>
</avue-crud>
<script>
export default {
 data() {
    return {
      option1:{
        expand: true,
        expandRowKeys:[],
        rowKey:'id',
        column: [{
          label: '姓名',
          prop: 'name'
        }, {
          label: '年龄',
          prop: 'sex'
        }]
      },
      data: [{
        id: 1,
        name: '张三',
        sex: 12,
      }, {
        id: 2,
        name: '李四',
        sex: 20,
      }]
    }
  }, 
  methods: {
   expandChanges(row, expendList) {
      if (expendList.length) {
        this.option1.expandRowKeys = []
        if (row) {
          this.option1.expandRowKeys.push(row.id)
        }
      } else {
        this.option1.expandRowKeys = []
      }
      this.$message.success('展开回调')
    }
  }
}
</script>

```
:::

