# 自定义表头
:::tip
 1.0.8+
::::

## 普通用法
:::demo 
```html

<avue-crud ref="crud" :option="option" :data="data">
  <template slot="nameHeader" slot-scope="{column}">
      <el-tag>{{(column || {}).label}}-{{(column || {}).prop}}</el-tag>
  </template>
</avue-crud>
<script>
export default {
    data() {
      return {
        option: {
          column: [{
            label: '姓名',
            prop: 'name',
            headerslot:true,
          }, {
            label: '年龄',
            prop: 'sex'
          }]
        },
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
      }
    }
}
</script>

```
:::
