# 防重提交
:::tip
 1.0.0+
::::

## 普通用法

:::demo  为了防止数据重复提交，加入了防重提交机制，`rowSave`方法和`rowUpdate`方法返回`done`用于关闭表单方法和`loading`用于关闭禁止的表单不关闭表单
```html
<avue-crud :data="data" :option="option" @row-save="handleRowSave" @row-update="handleRowUpdate"></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods:{
      handleRowSave(row,done,loading){
        this.$message.success('1秒后关闭禁止表单')
        setTimeout(()=>{
          loading()
          this.$message.success('3秒后关闭表单')
        },1000)
        setTimeout(()=>{
            done()
        },3000)
      }, 
      handleRowUpdate(row,index,done,loading){
        this.$message.success('1秒后关闭禁止表单')
        setTimeout(()=>{
          loading()
          this.$message.success('3秒后关闭表单')
        },1000)
        setTimeout(()=>{
            done()
        },3000)
      }
    }
}
</script>
```
:::

