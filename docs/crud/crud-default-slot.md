# 自定义其它


:::tip
 2.7.9+
::::

:::demo
```html
<avue-crud :option="option" :page.sync="page" :data="data">
  <template slot="header">
    <el-tag size="small">头部卡槽</el-tag>
  </template>
  <template slot="footer">
    <el-tag size="small">尾部卡槽</el-tag>
  </template>
   <template slot="page">
    <el-tag size="small">分页卡槽</el-tag>
  </template>
</avue-crud>
<script>
export default {
  data(){
    return {
       page:{
         total:40
       },
       data:[{
         name:'张三',
         age:18,
       }],
       option:{
          column: [{
            label: '姓名',
            prop: 'name',
            search:true
          },{
            label: '年龄',
            prop: 'age',
            search:true
          }]
       }
    }
  }
}
</script>

```
:::



