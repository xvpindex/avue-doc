# 多选
:::tip
 1.0.0+
::::

## 普通用法

:::demo  设`selection`属性为`true`即可；勾选的同时会回调`selectionChange`方法返回当前选中的数据,`setCurrent`方法设置选中的行,`selectable`函数决定该行是否可以勾选
```html

<avue-crud ref="crud" :data="data" :option="option" @selection-change="selectionChange">
   <template slot="menuLeft" slot-scope="{size}">
    <el-button type="success" icon="el-icon-check" :size="size" @click="toggleSelection([data[1]])">选中第二行</el-button>
    <el-button type="danger" icon="el-icon-delete" :size="size" @click="toggleSelection()">取消选择</el-button>
  </template>
</avue-crud>
<script>
export default {
    data() {
      return {
        data: [{
          id:1,
          name: '张三',
          sex: '男'
        },{
          id:2,
          name: '李四',
          sex: '女'
        }],
        option:{
          selection: true,
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
      selectionChange(list){
        this.$message.success('选中的数据'+ JSON.stringify(list));
      },
      toggleSelection(val){
        this.$refs.crud.toggleSelection(val);
      }
    }
}
</script>
```
:::


## 禁止选择

:::demo `selectable`函数决定该行是否可以勾选
```html

<avue-crud ref="crud" :data="data" :option="option" @selection-change="selectionChange" ></avue-crud>
<script>
export default {
    data() {
      return {
        data: [{
          id:1,
          name: '张三',
          sex: '男'
        },{
          id:2,
          name: '李四',
          sex: '女'
        }],
        option:{
          selection: true,
          selectable:(row,index)=>{
            return index===1;
          },
          tip:false,
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
      selectionChange(list){
        this.$message.success('选中的数据'+ JSON.stringify(list));
      }
    }
}
</script>
```
:::


## 翻页多选

:::demo  设置`reserveSelection`为`true`保留之前的勾选
```html

<avue-crud  :page.sync="page" :data="data" :option="option" @selection-change="selectionChange" @on-load="onLoad">
</avue-crud>
<script>
export default {
    data() {
      return {
         page: {
          pageSize: 2,
          pageSizes:[2]
        },
        data: [],
        option:{
          selection: true,
          reserveSelection:true,
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
      onLoad(page) {
        this.page.total = 4
        //模拟分页
        if (this.page.currentPage === 1) {
          this.data = [
            {
              id:1,
              name: '张三',
              sex: '男'
            },{
              id:2,
              name: '李四',
              sex: '女'
            }
          ]
        } else if (this.page.currentPage == 2) {
          this.data = [
            {
              id:3,
              name: '王五',
              sex: '女'
            },{
              id:4,
              name: '赵六',
              sex: '女'
            }
          ]
        }
      },
      selectionChange(list){
        this.$message.success('选中的数据'+ JSON.stringify(list));
      }
    }
}
</script>
```
:::

## 提示

:::demo 设置`tip`为`false`可以取消表格上方显示的提示，同时支持对应的卡槽自定义
```html

<avue-crud ref="crud" :data="data" :option="option" @selection-change="selectionChange">
   <template slot="tip">
    <el-tag type="danger" size="mini">自定义内容</el-tag>
  </template>
</avue-crud>
<script>
export default {
    data() {
      return {
        data: [{
          id:1,
          name: '张三',
          sex: '男'
        },{
          id:2,
          name: '李四',
          sex: '女'
        }],
        option:{
          selection: true,
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    }
}
</script>
```
:::