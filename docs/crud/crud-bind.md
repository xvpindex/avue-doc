# 深结构绑定
:::tip
 2.6.11+
::::

## 普通用法 
:::demo  `bing`绑定的对象数据是双向的，改变任意一个，另外一个也会改变，可以无限深结构
```html
<avue-crud :option="option" :data="data" v-model="form">
  <div slot-scope="{}" slot="bindForm">
    <el-button type="primary" size="small" @click="form.deep.deep.deep.value='改变deep.deep.deep.value值'">改变deep.deep.deep.value值</el-button>
    <el-button type="primary" size="small" @click="form.test='改变test值'">改变test值</el-button>
    </br></br>
    {{form}}
  </div>
</avue-crud>

<script>
export default{
  data() {
    return {
      form:{},
      data: [{ 
        deep:{
          deep:{
            deep:{
              value:'我是深结构'
            }
          }
        }
      }],
      option: {
        labelWidth: 120,
        column: [
          {
            label: '深结构',
            prop: 'test',
            bind:'deep.deep.deep.value'
          },{
            label: '',
            prop: 'bind',
            span:24,
            formslot:true
          }
        ]
      }
    }
  }
}
</script>

```
:::
