# 字典

:::tip
 1.0.0+
::::
- dicFlag配置为true，打开表单时候会重新拉去字段，可以查看network

## 普通用法

:::demo 本地字典只要配置`dicData`为一个`Array`数组即可，便会自动加载字典到对应的组件中，注意返回的字典中value类型和数据的类型必须要对应，比如都是字符串或者都是数字。
```html
<avue-crud :data="data" :option="option" ></avue-crud>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男',
            grade:1,
            cascader:[0,1],
            checkbox:[0,1],
            tree:0,
            province: '110000',
            city: '110000'
          }, {
            name:'李四',
            sex:'女',
            grade:0,
            cascader:[0,2],
            checkbox:[0,1],
            tree:1,
            province: '130000',
            city: '130000'
          }
        ],
        option:{
          column:[
             {
              label:'姓名',
              prop:'name',
            }, {
              label:'性别',
              prop:'sex'
            },{
              label:'权限',
              prop:'grade',
              type:'select',
              dicData:[
                {
                  label:'有权限',
                  value:1
                },{
                  label:'无权限',
                  value:0
                },{
                  label:'禁止项',
                  disabled:true,
                  value:-1
                }
              ]
            },{
              label:'级别',
              prop:'cascader',
              type:'cascader',
              dicData:[{
                label:'一级',
                value:0,
                children:[
                  {
                    label:'一级1',
                    value:1,
                  },{
                    label:'一级2',
                    value:2,
                  }
                ]
              }],
            },
            {
              width:120,
              label:'多选',
              prop:'checkbox',
              type:'checkbox',
              span:24,
              dicData:[{
                label:'有权限',
                value:1
              },{
                label:'无权限',
                value:0
              },{
                label:'禁止项',
                disabled:true,
                value:-1
              }]
            },
            {
              label:'树型',
              prop:'tree',
              type:'tree',
              dicData:[{
                label:'一级',
                value:0,
                children:[
                  {
                    label:'一级1',
                    value:1,
                  },{
                    label:'一级2',
                    value:2,
                  }
                ]
              }]
            },{
              label:'城市',
              prop:'province',
              type:'select',
              dicFlag:true,
              props: {
                  label: 'name',
                  value: 'code'
              },
              dicUrl:`${baseUrl}/getProvince`
            },{
              label:'城市树',
              prop:'city',
              type:'tree',
              props: {
                  label: 'name',
                  value: 'code'
              },
              dicUrl:`${baseUrl}/getProvince`
            },
          ]
        }
      }
    }
}
</script>
```
:::



## 后台接口列字典

:::demo 如果你的后台字典不是本地字典，支持给每一列属性单独配置网络字典,如果是级联字典，需要调用内部`dicInit`方法去加载

```html
<avue-crud ref="crud" :data="data" :option="option" ></avue-crud>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男',
            province: '110000',
            city: '110100',
            area: '110101',
          }, {
            name:'李四',
            sex:'女',
            province: '130000',
            city: '130200',
            area: '130202',
          }
        ],
        option:{
          column:[
             {
              label:'姓名',
              prop:'name',
            }, {
              label:'性别',
              prop:'sex'
            },{
              label:'城市',
              prop:'province',
              type:'select',
              cascaderItem: ['city', 'area'],
              props: {
                  label: 'name',
                  value: 'code'
              },
              dicUrl:`${baseUrl}/getProvince`
            },
            {
              width: 120,
              label: '城市',
              prop: 'city',
              type: 'select',
              cell: true,
              props: {
                label: 'name',
                value: 'code'
              },
              dicUrl: `${baseUrl}/getCity/{{key}}`,
              rules: [
                {
                  required: true,
                  message: '请选择城市',
                  trigger: 'blur'
                }
              ]
            },
            {
              width: 120,
              label: '地区',
              prop: 'area',
              cell: true,
              props: {
                label: 'name',
                value: 'code'
              },
              type: 'select',
              dicUrl: `${baseUrl}/getArea/{{key}}`,
              rules: [
                {
                  required: true,
                  message: '请选择地区',
                  trigger: 'blur'
                }
              ]
            }
          ]
        }
      }
    },
    mounted(){
      this.$refs.crud.dicInit('cascader');
    }
}
</script>
```
:::