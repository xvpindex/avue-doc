# 表单拖拽
- 点击新增或则编辑即可拖动表单
- 同时已经将拖拽封装成指令，在el的原声dialog上添加v-dialogdrag即可拖拽
:::tip
 2.5.3+
::::


## 普通用法

:::demo `dialogDrag`设置为`true`即可拖动表单
```html
<avue-crud :data="data" :option="option"></avue-crud>
<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:true
          }, {
            name:'李四',
            sex:false
          }
        ],
        option:{
          dialogDrag:true,
          column:[
             {
              label:'姓名',
              prop:'name',
            }, {
              label:'性别',
              prop:'sex',
              type:'switch',
              dicData:[{
                label:'男',
                value:true
              },{
                label:'女',
                value:false
              }]
            }
          ]
        }
      }
    }
}
</script>


```
:::


