# 修改对象属性
:::tip
 2.8.12+
::::

## 普通用法
:::demo  
```html
<avue-crud :defaults.sync="defaults" :data="data" :option="option"  v-model="form"></avue-crud>
<script>
export default {
  data(){
    return {
       defaults:{},
       data:[{text1:0}],
       form:{},
       option:{
          column: [{
            label: '内容1',
            prop: 'text1',
            type:'radio', 
            dicData:[{
              label:'显示',
              value:0
            },{
              label:'隐藏',
              value:1,
            }]
          },{
            label: '内容2',
            prop: 'text2',
            display:true
          },{
            label: '内容3',
            prop: 'text3'
          }]
       }
    }
  },
  watch:{
    'form.text1'(val){
      if(val==0){
        this.defaults.text2.display=true
        this.defaults.text3.label='内容3'
      }else{
        this.defaults.text2.display=false
        this.defaults.text3.label='有没有发现我变了'
      }
    }
  }
}
</script>

```
:::

