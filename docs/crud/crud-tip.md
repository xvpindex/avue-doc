# 多选提示

当配置selection为true时启动tip操作栏目

## 普通用法
:::demo  `tip`属性可以控制是否启动操作栏,同时开启自定义`tip`卡槽，可以灵活书写逻辑
```html
<avue-crud :option="option" :data="data">
  <template slot="tip">
    <el-button type="text" size="small">
      自定义按钮
    </el-button>
    <span>自定义内容</span>
  </template>
</avue-crud>
<script>
export default {
  data(){
    return {
       data:[
         {
           name:'张三'
         }, {
           name:'李四'
         }
       ],
       option:{
          index:true,
          selection: true,
          border:true,
          column: [{
            label: '姓名',
            prop: 'name'
          }]
       }
    }
  }
}
</script>

```
:::
