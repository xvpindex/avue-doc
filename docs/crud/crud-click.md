# 行单击 | 双击编辑
:::tip
 2.0.5+
::::

## 行单击编辑


:::demo 调用行单击或双击事件，在调用内部`rowEdit`方法即可
```html
<avue-crud ref="crud" :data="data" :option="option" @row-click="handleRowClick"></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          menu:false,
          dialogType:'drawer',
          dialogWidth:800,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        }
      };
    },
    methods: {
        handleRowClick (row, event, column) {
          this.$refs.crud.rowEdit(row,row.$index);
        },
    }
}
</script>
```
:::

## 行双击编辑

:::demo 
```html
<avue-crud ref="crud" :data="data" :option="option" @row-dblclick="handleRowDBLClick"></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          menu:false,
          dialogType:'drawer',
          dialogWidth:800,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        }
      }
    },
    methods: {
       handleRowDBLClick (row, event) {
        this.$refs.crud.rowEdit(row,row.$index);
      },
    }
}
</script>
```
:::

