# 表格筛选
:::tip
 1.0.0+
::::


## 普通用法
:::demo  设置`filters`为`true`，字典用法和普通用法一致,`filterMethod`为自定义的筛选逻辑，`filter-multiple`筛选的数据为多选还是单选，默认为 `true`
```html
<avue-crud :data="data" :option="option"></avue-crud>
<script>
export default {
 data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          column:[
             {
              label:'姓名',
              prop:'name',
              formatter:function(row, value , label, column){
                  return row.name +'自定义'
              }
            }, {
              label:'性别',
              prop:'sex',
              filters:true,
              dicData:[{ label: '男', value: '男' }, { label: '女', value: '女' }],
              filterMethod:function(value, row, column) {
                return row.sex === value;
              }
            }
          ]
        }
      }
    }
  }
</script>

```
:::

