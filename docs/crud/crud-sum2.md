# 行 | 列合并

配置相应的属性后，在表格末尾即可达到统计合计功能

## 行合并

:::demo 通过给传入`spanMethod`方法可以实现合并行或列，方法的参数是一个对象，里面包含当前行`row`、当前列`column`、当前行号`rowIndex`、当前列号`columnIndex`四个属性。该函数可以返回一个包含两个元素的数组，第一个元素代表`rowspan`，第二个元素代表`colspan`。 也可以返回一个键名为`rowspan`和`colspan`的对象。

```html
<avue-crud
  :data="data"
  :option="option"
  :span-method="spanMethod"
></avue-crud>

<script>
  export default {
    data() {
      return {
        data: [
          {
            id: '12987122',
            name: '王小虎',
            amount1: '234',
            amount2: '3.2',
            amount3: 10
          },
          {
            id: '12987123',
            name: '王小虎',
            amount1: '165',
            amount2: '4.43',
            amount3: 12
          },
          {
            id: '12987124',
            name: '王小虎',
            amount1: '324',
            amount2: '1.9',
            amount3: 9
          },
          {
            id: '12987125',
            name: '王小虎',
            amount1: '621',
            amount2: '2.2',
            amount3: 17
          },
          {
            id: '12987126',
            name: '王小虎',
            amount1: '539',
            amount2: '4.1',
            amount3: 15
          }
        ],
        option: {
          border: true,
          menu:false,
          column: [
            {
              label: 'ID',
              prop: 'id'
            },
            {
              label: '姓名',
              prop: 'name'
            },
            {
              label: '数值 1',
              prop: 'amount1'
            },
            {
              label: '数值 2',
              prop: 'amount2'
            },
            {
              label: '数值 3',
              prop: 'amount3'
            }
          ]
        }
      }
    },
    methods: {
      spanMethod({ row, column, rowIndex, columnIndex }) {
        if (rowIndex % 2 === 0) {
          if (columnIndex === 0) {
            return [1, 2]
          } else if (columnIndex === 1) {
            return [0, 0]
          }
        }
      }
    }
  }
</script>
```

:::

## 列合并

:::demo 通过给传入`spanMethod`方法可以实现合并行或列，方法的参数是一个对象，里面包含当前行`row`、当前列`column`、当前行号`rowIndex`、当前列号`columnIndex`四个属性。该函数可以返回一个包含两个元素的数组，第一个元素代表`rowspan`，第二个元素代表`colspan`。 也可以返回一个键名为`rowspan`和`colspan`的对象。

```html
<avue-crud
  :data="data"
  :option="option"
  :span-method="spanMethod"
></avue-crud>

<script>
  export default {
    data() {
      return {
        data: [
          {
            id: '12987122',
            name: '王小虎',
            amount1: '234',
            amount2: '3.2',
            amount3: 10
          },
          {
            id: '12987123',
            name: '王小虎',
            amount1: '165',
            amount2: '4.43',
            amount3: 12
          },
          {
            id: '12987124',
            name: '王小虎',
            amount1: '324',
            amount2: '1.9',
            amount3: 9
          },
          {
            id: '12987125',
            name: '王小虎',
            amount1: '621',
            amount2: '2.2',
            amount3: 17
          },
          {
            id: '12987126',
            name: '王小虎',
            amount1: '539',
            amount2: '4.1',
            amount3: 15
          }
        ],
        option: {
          border: true,
          menu:false,
          column: [
            {
              label: 'ID',
              prop: 'id'
            },
            {
              label: '姓名',
              prop: 'name'
            },
            {
              label: '数值 1',
              prop: 'amount1'
            },
            {
              label: '数值 2',
              prop: 'amount2'
            },
            {
              label: '数值 3',
              prop: 'amount3'
            }
          ]
        }
      }
    },
    methods: {
      spanMethod({ row, column, rowIndex, columnIndex }) {
        if (columnIndex === 0) {
          if (rowIndex % 2 === 0) {
            return {
              rowspan: 2,
              colspan: 1
            }
          } else {
            return {
              rowspan: 0,
              colspan: 0
            }
          }
        }
      }
    }
  }
</script>
```

:::


## 动态合并

:::demo 

```html
<avue-crud
  :data="data"
  :option="option"
  :span-method="spanMethod"
></avue-crud>

<script>
  export default {
    data() {
      return {
        data: [
          {
            name: '王小虎',
            amount1: '234',
            amount2: '3.2',
            amount3: 10
          },
          {
            name: '王小虎',
            amount1: '165',
            amount2: '4.43',
            amount3: 12
          },
          {
            name: '王小虎',
            amount1: '324',
            amount2: '1.9',
            amount3: 9
          },
          {
            name: '王小虎',
            amount1: '621',
            amount2: '2.2',
            amount3: 17
          },
          {
            name: '王小虎',
            amount1: '539',
            amount2: '4.1',
            amount3: 15
          },
          {
            name: '王大虎',
            amount1: '539',
            amount2: '4.1',
            amount3: 15
          }
        ],
        option: {
          border: true,
          column: [
            {
              label: '姓名',
              prop: 'name'
            },
            {
              label: '数值 1',
              prop: 'amount1'
            },
            {
              label: '数值 2',
              prop: 'amount2'
            },
            {
              label: '数值 3',
              prop: 'amount3'
            }
          ]
        },
        spanArr:[],
        key:'name'
      }
    },
    created(){
        this.rowspan()
    },
    methods: {
      rowspan() {
       this.spanArr=[];
       this.position=0;
       this.data.forEach((item,index)=>{
         if(index===0){
           this.spanArr.push(1)
           this.position=0;
         }else{
           if(this.data[index][this.key]===this.data[index-1][this.key]){
             this.spanArr[this.position] +=1;
             this.spanArr.push(0)
           }else{
             this.spanArr.push(1)
             this.position=index
           }
         }
       })
      },
      spanMethod({ row, column, rowIndex, columnIndex }) {
        if(column.property==[this.key]){
          const _row=this.spanArr[rowIndex];
          const _col=_row>0?1:0;
          return {
            rowspan:_row,
            colspan:_col
          }
        }
        
      }
    }
  }
</script>
```

:::