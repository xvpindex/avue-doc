# 搜索


:::tip
 2.1.0+
::::

## 普通用法
:::demo  `searchSpan`搜索框的宽度，`searchRange`配置后可以开启范围搜索（date,datetime,time）,当然你可能首次打开不想显示搜索可以配置`searchShow`为`false`,点击搜索小图标即可展开,`searchValue`为搜索的默认值
```html
{{search}}
<avue-crud :option="option" :search.sync="search" :data="data" @search-change="searchChange">
  <template slot="searchMenu"  slot-scope="{row,size}">
      <el-button :size="size" @click="searchSubmit(row)">自定义提交</el-button>
  </template>
  <template slot="search" slot-scope="{row,size}">
    <el-input placeholder="自定义输入框" :size="size" style="width:200px" v-model="search.slot"></el-input>
  </template>
</avue-crud>
<script>
export default {
  data(){
    return {
       search:{},
       data:[{
         name:'张三'
       }],
       option:{
          searchMenuPosition:'right',
          column: [{
            label: '姓名',
            prop: 'name',
            searchValue:'small',
            search:true,
            searchRules: [{
              required: true,
              message: "请输入姓名",
              trigger: "blur"
            }],
          },{
            label: '日期',
            prop: 'date',
            type:'datetime',
            searchSpan:12,
            searchRange:true,
            search:true,
          }]
       }
    }
  },
  methods:{
      searchSubmit(item){
        this.$message.success(JSON.stringify(item))
      },
      searchChange(params,done) {
        done();
        this.$message.success(JSON.stringify(params));
      }
  }
}
</script>

```
:::


## 局部展开收缩
:::demo  `searchIcon`是否启用功能按钮, `searchIndex`配置收缩展示的个数,`searchShowBtn`隐藏搜索显隐藏按钮
```html
<avue-crud :option="option2" :data="data0" @search-change="searchChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data0:[
         {
          text1:'文本1',
          text2:'文本2'
         }
       ],
       option2:{
          // searchIndex:3,
          searchIcon:true,
          searchShowBtn:false,
          column: [{
            label: '内容1',
            prop: 'text1',
            search:true,
          },{
            label: '内容2',
            prop: 'text2',
            search:true,
          },{
            label: '内容3',
            prop: 'text3',
            search:true,
          },{
            label: '内容4',
            prop: 'text4',
            search:true,
          }]
       }
    }
  },
  methods:{
    searchChange(params,done) {
      done();
      this.$message.success(JSON.stringify(params));
    }
  }
}
</script>

```
:::



## 辅助提示语
:::demo  前提的`column`中要启动搜索的字典`search`设置为`true`,`searchTip`为提示的内容,`searchTipPlacement`为提示语的方向，默认为`bottom`
```html
<avue-crud :option="option0" :data="data0" @search-change="searchChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data0:[
         {
          text1:'文本1',
          text2:'文本2'
         }
       ],
       option0:{
          column: [{
            label: '内容1',
            prop: 'text1',
            search:true,
            searchTip:'我是一个默认提示语',
          },{
            label: '内容2',
            prop: 'text2',
            search:true,
            searchTip:'我是一个左边提示语',
            searchTipPlacement:'left',
          }]
       }
    }
  },
  methods:{
      searchChange(params,done) {
        done();
        this.$message.success(JSON.stringify(params));
      }
  }
}
</script>

```
:::


## 按钮是否单独成行
:::demo  前提的`searchMenuSpan`可以控制搜索按钮的长度
```html
<avue-crud :option="option1" :data="data0" @search-change="searchChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data0:[
         {
          text1:'文本1',
          text2:'文本2'
         }
       ],
       option1:{
          searchMenuSpan:8,
          column: [{
            label: '内容1',
            prop: 'text1',
            search:true,
          },{
            label: '内容2',
            prop: 'text2',
            search:true,
          }]
       }
    }
  },
  methods:{
      searchChange(params,done) {
        done();
        this.$message.success(JSON.stringify(params));
      }
  }
}
</script>

```
:::


## 多级联动

:::tip
 2.2.3+
::::

:::demo  `cascaderItem`为需要联动的子选择框`prop`值，注意删除key之间的空格,表格字典翻译数据必须是异步
```html
<avue-crud ref="crud" :option="option" :data="data" @search-change="searchChange" @submit="handleSubmit"></avue-crud>
<script>
var baseUrl = 'https://cli.avuejs.com/api/area'
export default {
  data(){
    return {
       data:[],
       option:{
          excelBtn:true,
          column: [{
            label: '省份',
            prop: 'province',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            cascaderItem: ['city', 'area'],
            search:true,
            dicUrl: `${baseUrl}/getProvince`,
            rules: [
              {
                required: true,
                message: '请选择省份',
                trigger: 'blur'
              }
            ]
          },
          {
            label: '城市',
            prop: 'city',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            search:true,
            dicUrl: `${baseUrl}/getCity/{{key}}`,
            rules: [
              {
                required: true,
                message: '请选择城市',
                trigger: 'blur'
              }
            ]
          },
          {
            label: '地区',
            prop: 'area',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            search:true,
            dicUrl: `${baseUrl}/getArea/{{key}}`,
            rules: [
              {
                required: true,
                message: '请选择地区',
                trigger: 'blur'
              }
            ]
          }]
       }
    }
  },
  mounted() {
    this.data = [
      {
        id: 1,
        name: '张三',
        province: '110000',
        city: '110100',
        area: '110101',
      },
      {
        id: 2,
        name: '李四',
        province: '140000',
        city: '140600',
        area: '140623'
      }
    ]
    this.$nextTick(()=>this.$refs.crud.dicInit('cascader'))
  },
  methods:{
    searchChange(form,done){
       setTimeout(()=>{
          this.$message.success(JSON.stringify(form))
         done()
       },500)
    }
  }
}
</script>

```
:::


