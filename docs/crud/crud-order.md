# 字段排序
:::tip
2.6.16+
::::

## 普通用法

:::demo  设`order`和`searchOrder`属性可与排序表单字段
```html
<avue-crud :data="data" :option="option" ></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name',
              search:true,
            }, {
              label:'性别',
              prop:'sex',
              search:true,
              searchOrder:1,
              order:1
            }
          ]
        },
      };
    },
    methods: {
    }
}
</script>
```
:::