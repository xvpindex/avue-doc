# 复制数据
:::tip
 2.6.14+
::::

## 普通用法

:::demo 设置`copyBtn`为`true`时激活行复制功能,复制的数据会去除`rowKey`配置的主键
```html
<avue-crud :data="data" :option="option" v-model="form"></avue-crud>
{{form}}

<script>
export default {
    data() {
      return {
        form:{},
        data: [
          {
            ids:1,
            name:'张三',
            sex:'男'
          }, {
            ids:2,
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          rowKey:'ids',
          copyBtn:true,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    }
}
</script>
```
:::