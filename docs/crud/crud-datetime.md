# 日期组件
:::tip
 2.2.2+
::::

## 普通用法 
:::demo 配置`dateBtn`为`true`即可激活,默认不显示
```html
<avue-crud ref="crud" @date-change="dateChange":option="option" :data="data" ></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       },{
          text1:'内容3-1',
          text2:'内容3-2'
       }],
       option:{
          dateBtn:true,
          align:'center',
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  },
  methods:{
    dateChange(date){
      this.$message.success(JSON.stringify(date));
    }
  }
}
</script>

```
:::




