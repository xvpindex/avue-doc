# 行拖拽排序
:::tip
 2.0.2+
::::

```
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://cdn.staticfile.org/Sortable/1.10.0-rc2/Sortable.min.js"></script>
```

## 普通用法
:::demo  `sortable`设置为`true`即可开启拖拽功能，`sortable-change`为拖拽后的回调方法,拖拽后可看控制台打印日志
```html
<avue-crud :option="option" :data="data" @sortable-change="sortableChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       },{
          text1:'内容3-1',
          text2:'内容3-2'
       },{
          text1:'内容4-1',
          text2:'内容4-2'
       },{
          text1:'内容5-1',
          text2:'内容5-2'
       }],
       option:{
          sortable:true,
          addBtn:false,
          menu:false,
          align:'center',
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  },
  methods:{
     sortableChange(oldindex, newindex, row, list) {
        this.data=[]
        this.$nextTick(()=>{
          this.data=list;
        })
       console.log(oldindex, newindex, row, list)
      }
  }
}
</script>

```
:::


## 单独拖拽列
:::demo  `dragHandler`设置为`true`即可开启列拖拽功能，其他列不可拖动
```html
<avue-crud :option="option1" :data="data" @sortable-change="sortableChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       },{
          text1:'内容3-1',
          text2:'内容3-2'
       },{
          text1:'内容4-1',
          text2:'内容4-2'
       },{
          text1:'内容5-1',
          text2:'内容5-2'
       }],
       option1:{
          sortable:true,
          dragHandler:true,
          addBtn:false,
          menu:false,
          align:'center',
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  },
  methods:{
     sortableChange(oldindex, newindex, row, list) {
        this.data=[]
        this.$nextTick(()=>{
          this.data=list;
        })
       console.log(oldindex, newindex, row, list)
      }
  }
}
</script>

```
:::



