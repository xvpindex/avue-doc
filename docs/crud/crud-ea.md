# 新增编辑状态
:::tip
 2.6.7+
::::
- 点击新增和编辑去观察区别

## 普通用法

:::demo 
```html
<avue-crud :data="data" :option="option" ></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男',
            grade:0
          }, {
            name:'李四',
            sex:'女',
            grade:1
          }
        ],
        option:{
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name',
              editDisabled:true
            }, {
              label:'性别',
              prop:'sex',
              editDisplay:false
            }, {
              label:'权限',
              prop:'grade',
              editDetail:true,
              addDisabled:true
            }, {
              label:'测试',
              prop:'test',
              addDisplay:false
            }
          ]
        },
      };
    }
}
</script>
```
:::
