# 刷新
:::tip
 1.0.0+
::::

## 普通用法

:::demo 点击刷新按钮回调`refresh-change`方法
```html
<avue-crud :option="option" :data="data" :page.sync="page" :search.sync="search" @refresh-change="refreshChange"></avue-crud>

<script>
export default {
    data() {
      return {
        page:{
          total:20
        },
        search:{},
        data: [
          {
            name:'张三',
            sex:'男',
            search:true,
            searchValue:'测试'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    },
    methods: {
     refreshChange(){
        this.$message({
          message: `当前的返回的分页对象${JSON.stringify(this.page)} 搜索表单对象${JSON.stringify(this.search)}`,
          type: 'success',
        });
     }
    }
}
</script>
```
:::