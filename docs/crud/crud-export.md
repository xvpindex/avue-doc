# 表格导出

常用的表格导出功能，如果开启此功能需要在项目中加入依赖包，建议采用cdn的形式

```html
<!-- 导入需要的包 （一定要放到index.html中的head标签里）-->
<script src="https://cdn.staticfile.org/FileSaver.js/2014-11-29/FileSaver.min.js"></script>
<script src="https://avuejs.com/cdn/xlsx.full.min.js"></script>
```

## 普通用法
:::demo  `excelBtn`设置为`true`即可开启导出功能，如果配置了`selection`属性，则需要勾选导出的数据，否则为全部,如果配置了`title`属性，则为导出文件的名称
```html
<avue-crud :option="option" :data="data"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       }],
       option:{
          title:'表格的标题',
          excelBtn:true,
          addBtn:false,
          menu:false,
          align:'center',
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  }
}
</script>

```
:::


