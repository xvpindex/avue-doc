# 基础

:::tip
 表格中的弹出的表单是内置组件`avue-form`组件,配置属性可以参考[Form组件文档](/form/form)
::::

## 普通用法

:::demo  `data`数据的对象数组，`option`为表格要配置的数据列，`v-model`为当前编辑或者新增的表单对象，自动根据`option`中的`column`配置去加载对象注入进去
```html
<avue-crud :data="data" :option="option" v-model="form"></avue-crud>

<script>
export default {
 data() {
      return {
        form:{},
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }, {
            name:'王五',
            sex:'女'
          }, {
            name:'赵六',
            sex:'男'
          }
        ],
        option:{
          align:'center',
          menuAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            },
            {
              label:'性别',
              prop:'sex'
            }
          ]
        }
      }
    }
  }
</script>
``` 
:::

## 丰富表格

该示例主要展示了表格丰富的显示效果。

:::demo  
```html
<el-row style="margin-bottom:20px;font-size:15px">
  <el-col :span="4">显示边框: <el-switch size="small" v-model="showBorder"> </el-switch></el-col>
  <el-col :span="4">显示斑马纹: <el-switch size="small" v-model="showStripe"> </el-switch></el-col>
  <el-col :span="4">显示索引: <el-switch size="small" v-model="showIndex"> </el-switch></el-col>
  <el-col :span="4">显示多选框: <el-switch size="small" v-model="showCheckbox"> </el-switch></el-col>
  <el-col :span="4">显示表头: <el-switch size="small" v-model="showHeader"> </el-switch></el-col>
</el-row>
<el-row style="margin-bottom:20px">
   <el-radio-group v-model="sizeValue">
    <el-radio label="small">默认</el-radio>
    <el-radio label="medium">medium</el-radio>
    <el-radio label="small">small</el-radio>
    <el-radio label="mini">mini</el-radio>
  </el-radio-group>
</el-row>
<avue-crud :data="data" :option="option0"></avue-crud>

<script>
export default {
 data() {
      return {
        obj:{},
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }, {
            name:'王五',
            sex:'女'
          }, {
            name:'赵六',
            sex:'男'
          }
        ],
        showBorder: false,
        showStripe: false,
        showHeader: true,
        showIndex: true,
        showCheckbox: false,
        showPage:false,
        sizeValue:'small'
      }
    },
     computed: {
       option0(){
         return{
            border:this.showBorder,
            stripe:this.showStripe,
            showHeader:this.showHeader,
            index:this.showIndex,
            size:this.sizeValue,
            selection:this.showCheckbox,
            page:this.showPage,
            align:'center',
            menuAlign:'center',
            column:[
              {
                label:'姓名',
                prop:'name'
              },
              {
                label:'性别',
                prop:'sex'
              }
            ]
         }
       }
     }
  }
</script>
```
:::

## 合并菜单

:::demo  配置`menuType`为`menu`表格的操作栏目菜单合并，`menuBtn`卡槽为自定义卡槽,`delBtn`和`editBtn`会消失,`dateBtn`控件的`dateDefault`为`true`时首次进来会加载回调方法,
```html
<avue-crud :data="data" :option="option1" v-model="obj" @date-change="dateChange">
  <template slot-scope="scope" slot="menuBtn">
     <el-dropdown-item divided @click.native="tip">自定义按钮</el-dropdown-item>
  </template>
  <template slot-scope="scope" slot="menu" >
     <el-button style="margin-left:10px;" size="small" type="text" icon="el-icon-user" @click.native="tip">自定义按钮</el-button>
  </template>
</avue-crud>

<script>
export default {
 data() {
      return {
        obj:{},
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }, {
            name:'王五',
            sex:'女'
          }, {
            name:'赵六',
            sex:'男'
          }
        ],
        option1:{
          menuType:'menu',
          menuBtnTitle:'自定义名称',
          dateBtn:true,
          dateDefault:true,
          column:[
             {
              label:'姓名',
              prop:'name'
            },
            {
              label:'性别',
              prop:'sex'
            }
          ]
        }
      }
    },
    methods: {
      dateChange(date){
        this.$message.success(date);
      },
      tip(){
        this.$message.success('自定义按钮');
      }
    }
  }
</script>
``` 
:::


## 图标菜单

:::demo  配置`menuType`为`icon`时表格操作栏为图标按钮
```html
<avue-crud :data="data" :option="option3" v-model="obj" @date-change="dateChange">
  <template slot-scope="scope" slot="menu">
     <el-button size="small" @click.native="tip" icon="el-icon-share"></el-button>
  </template>
</avue-crud>

<script>
export default {
 data() {
      return {
        obj:{},
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }, {
            name:'王五',
            sex:'女'
          }, {
            name:'赵六',
            sex:'男'
          }
        ],
        option3:{
          menuType:'icon',
          dateBtn:true,
          dateDefault:true,
          column:[
             {
              label:'姓名',
              prop:'name'
            },
            {
              label:'性别',
              prop:'sex'
            }
          ]
        }
      }
    },
    methods: {
      dateChange(date){
        this.$message.success(date);
      },
      tip(){
        this.$message.success('自定义按钮');
      }
    }
  }
</script>
``` 
:::