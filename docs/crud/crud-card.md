# 卡片模式


:::tip
 2.7.9+
::::

## 普通用法
:::demo  
```html
<avue-crud :option="option"  :data="data" ></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
         name:'张三'
       }],
       option:{
          card:true,
          searchMenuSpan:6,
          column: [{
            label: '姓名',
            prop: 'name',
            search:true,
          },{
            label: '日期',
            prop: 'date',
            type:'datetime',
            search:true,
          }]
       }
    }
  }
}
</script>

```
:::
