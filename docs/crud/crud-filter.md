# 表格筛选
常用自定筛选条件
:::tip
 1.0.0+
::::

## 普通用法
:::demo  `filterBtn`默认为`true`，可以自定义过滤条件，根据`filter`函数回调
```html
<avue-crud :option="option" :data="data" @filter="filterChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       }],
       option:{
          filterBtn:true,
          align:'center',
          addBtn:false,
          menu:false,
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  },
  methods:{
    filterChange(result) {
      this.$message.success('过滤器' + JSON.stringify(result))
    },
  }
}
</script>

```
:::

